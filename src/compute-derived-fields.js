const { isEmpty, isNull, isUndefined } = require("lodash")
const { insolation, acYield, dcYield, cuf } = require("./derived-fields")
const { isNumber } = require("./helpers/numbers")

const derivedFieldHelperMapping = {
  'poa_insolation': {
    computeHandler: insolation,
    argumentFieldList: ['irradiance_vertical', 'packet_count']
  },
  'ghi_insolation': {
    computeHandler: insolation,
    argumentFieldList: ['irradiance_horizontal', 'packet_count']
  },
  'ac_yield': {
    computeHandler: acYield,
    argumentFieldList: ['energy|active_total_export_cumulative', '$dcCapacity']
  },
  'dc_yield': {
    computeHandler: dcYield,
    argumentFieldList: ['energy|active_total_export_cumulative', '$dcCapacity']
  },
  'cuf': {
    computeHandler: cuf,
    argumentFieldList: ['energy|active_total_export_cumulative', '$dcCapacity']
  },
}


const computeDerivedFields = (dataPoint, derivedFieldList, metaInfo) => {
  const { timestamp, ...parameterCollection } = dataPoint
  const derivedFieldCollection = {}

  derivedFieldList.forEach(field => {
    if (!derivedFieldHelperMapping[field]) return;
   
    const argumentFieldList = derivedFieldHelperMapping[field].argumentFieldList
    const args = []

    for (const argField of argumentFieldList) {
      const splitArgs = argField.split('|')
      let argumentValue = null

      for (let splitArg of splitArgs) {
        if (splitArg.startsWith('$')) {
          var argValue = metaInfo[splitArg.replace('$', '')]          
        } else {
          var argValue = parameterCollection[splitArg]
        }

        if (!isUndefined(argValue)) {
          argumentValue = argValue
          break;
        }
      }

      if (isNull(argumentValue)) return;

      args.push(argumentValue)
    }

    if (isNull(argValue)) return;

    const computeHandler = derivedFieldHelperMapping[field].computeHandler
    const value = computeHandler(...args) 

    derivedFieldCollection[field] = isNumber(value) ? value : null;
  })

  if (isEmpty(derivedFieldCollection)) return null;

  return { timestamp, ...derivedFieldCollection }
}


const computeDerivedData = (tsData, fieldsToComputeList, metaInfo = {}) => {
  return tsData.map(dataPoint => {
    return computeDerivedFields(dataPoint, fieldsToComputeList, metaInfo)
  }).filter(val => val)
}


module.exports = {
  computeDerivedFields,
  computeDerivedData
}