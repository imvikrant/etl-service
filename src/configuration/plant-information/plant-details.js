const { QueryTypes } = require('sequelize')
const { model } = require('../../utils/db')

let mapping = null;

const getPlantDetails = async ({ plants } = {}) => {
  const sQuery = `
    SELECT plant_id, ac_capacity, dc_capacity, timezone FROM tbl_plant 
    ${plants && plants.length ? `WHERE plant_id IN (${plants})`: ''}
  `

  const result = await model.query(sQuery, {
      type: QueryTypes.SELECT
  })
   
  const configObj = {}
	result.forEach(({ 
		plant_id        : plantId,
    ac_capacity: acCapacity,
    dc_capacity: dcCapacity,
    timezone,
	}) => {
		configObj[plantId] = {
			plantId,
      acCapacity,
      dcCapacity,
      timezone
		}
	})

	return configObj
}


const loadPlantDetails = async ({ plants }) => {
	const plantDetailsMapping = await getPlantDetails({ plants })
	mapping = plantDetailsMapping || {}
}


const getPlantInfo = ({plantId}) => {
	if (mapping === null) throw new Error('Plant Details is not initialized')
	return mapping[plantId]
}


module.exports = {
  loadPlantDetails,
	getPlantInfo,
}