const { QueryTypes } = require("sequelize");
const { model } = require("../../utils/db");

let mapping = null


const isValidWsId = (id) => {
	if (!id) return;
	return id.split('-').length === 2;
} 


const getPlantWsMapping = async ({ plants } = {}) => {
	const sQuery = `
		SELECT 
			plant_id,
			concat_ws('-', temp_ref_block_id, temp_ref_device_id) 			temp_ref_id,
			concat_ws('-', irr_ref_block_id, irr_ref_device_id) 				poa_ref_id,
			concat_ws('-', ghi_irr_ref_block_id, ghi_irr_ref_device_id) ghi_ref_id,
			rad_device_ref_id 																					rad_ref_id,
			ws_type,
			pr_type,
			calculation_type
		FROM 
			tbl_plant_ws_map	
		${plants && plants.length ? `WHERE plant_id IN (${plants})`: ''}
	`

	const result = await model.query(sQuery, {
		type: QueryTypes.SELECT
	})

	const configObj = {};

	result.forEach(({ 
		plant_id        : plantId,
		temp_ref_id     : tempRefId,
		ghi_ref_id      : ghiRefId,
		poa_ref_id      : poaRefId,
		rad_ref_id      : radRefId,
		ws_type         : wsType,
		pr_type         : prType,
		calculation_type: calculationType,
	}) => {
		configObj[plantId] = {
			plantId,
			tempRefId: isValidWsId(tempRefId)? tempRefId : null,
			ghiRefId : isValidWsId(ghiRefId) ? ghiRefId : null,
			poaRefId : isValidWsId(poaRefId) ? poaRefId : null,
			radRefId : isValidWsId(radRefId) ? radRefId : null,
			wsType   : wsType || null,
			prType   : prType || null,
			calculationType: calculationType || null,
		}
	})

	return configObj
}


const loadWsMapping = async ({ plants }) => {
	const wsMapping = await getPlantWsMapping({ plants })
	mapping = wsMapping || {}
}


const getWsConfig = ({plantId}) => {
	if (mapping === null) throw new Error('Ws Mapping is not initialized')
	return mapping[plantId]
}


module.exports = {
  loadWsMapping,
	getWsConfig,
}