const { loadDeviceInformation, getEquipment, getEquipmentList, hasDevice } = require('./device-information')
const { loadParameterInformation, getParameter } = require('./parameter-information');
const { loadPlantDetails, getPlantInfo } = require('./plant-information/plant-details');
const { loadWsMapping, getWsConfig } = require('./plant-information/plant-ws');
let flag = 0;

const loadConfiguration = async ({ plants } = {}) => {
  if (flag == 1) return;
  await loadDeviceInformation({ plants }) // This needs to be loaded at first
  loadParameterInformation({ plants })
  await loadWsMapping({ plants })
  await loadPlantDetails({ plants })
  flag = 1;
  console.log('Configuration loaded successfully!!!')
} 

module.exports = {
  loadConfiguration,
  getEquipmentList,
  getEquipment,
  getParameter,
  hasDevice,
  getWsConfig,
  getPlantInfo
}