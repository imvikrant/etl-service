const inverterParameterConfig = require('./inverter.json')
const mfmParameterConfig = require('./mfm.json')
const emParameterConfig = require('./energy-meter.json')
const wsParameterConfig = require('./weather-station.json')
const scbParameterConfig = require('./scb.json')
const annunciatorParameterConfig = require('./annunciator.json')
const breakerParameterConfig = require('./breaker.json')
const mpptParameterConfig = require('./mppt.json')
const relayParameterConfig = require('./relay.json')
const trackerParameterConfig = require('./tracker.json')
const soilSensorParameterConfig = require('./soil-sensor.json')
const upsParameterConfig = require('./ups.json')
const zmuParameterConfig = require('./zmu.json')
const transformerParameterConfig = require('./transformer.json')

const getParameterInfo = (device, parameter) => {
  switch(device) {
    case 'inverter'       : return inverterParameterConfig[parameter] || null;
    case 'mfm'            : return mfmParameterConfig[parameter] || null;
    case 'energy meter'   : return emParameterConfig[parameter] || null;
    case 'weather station': return wsParameterConfig[parameter] || null;
    case 'scb'            : return scbParameterConfig[parameter] || null;
    case 'annunciator'    : return annunciatorParameterConfig[parameter] || null;
    case 'breaker'        : return breakerParameterConfig[parameter] || null;
    case 'mppt'           : return mpptParameterConfig[parameter] || null;
    case 'relay'          : return relayParameterConfig[parameter] || null;
    case 'tracker'        : return trackerParameterConfig[parameter] || null;
    case 'zmu'            : return zmuParameterConfig[parameter] || null;
    case 'ups'            : return upsParameterConfig[parameter] || null;
    case 'transformer'    : return transformerParameterConfig[parameter] || null;
    case 'soil sensor'    : return soilSensorParameterConfig[parameter] || null;
    default               : return null;
  }
}


const getParameterList = (device) => {
  switch(device) {
    case 'inverter'       : return Object.keys( inverterParameterConfig ) || null;
    case 'mfm'            : return Object.keys( mfmParameterConfig ) || null;
    case 'energy meter'   : return Object.keys( emParameterConfig ) || null;
    case 'weather station': return Object.keys( wsParameterConfig ) || null;
    case 'scb'            : return Object.keys( scbParameterConfig ) || null;
    case 'annunciator'    : return Object.keys( annunciatorParameterConfig ) || null;
    case 'breaker'        : return Object.keys( breakerParameterConfig ) || null;
    case 'mppt'           : return Object.keys( mpptParameterConfig ) || null;
    case 'relay'          : return Object.keys( relayParameterConfig ) || null;
    case 'tracker'        : return Object.keys( trackerParameterConfig ) || null;
    case 'zmu'            : return Object.keys( zmuParameterConfig ) || null;
    case 'ups'            : return Object.keys( upsParameterConfig ) || null;
    case 'transformer'    : return Object.keys( transformerParameterConfig ) || null;
    case 'soil sensor'    : return Object.keys( soilSensorParameterConfig ) || null;
    default               : return null;
  }
}

module.exports = {
  getParameterInfo,
  getParameterList
}