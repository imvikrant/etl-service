const { hasDevice, getEquipmentList, getEquipment, loadDeviceInformation } = require('../device-information')
const inverterParameters = require('../parameter-information/parameters/inverter.json')
const mfmParameters = require('../parameter-information/parameters/mfm.json')
const weatherStationParameters = require('../parameter-information/parameters/weather-station.json')
const scbParameters = require('../parameter-information/parameters/scb.json')
const emParameters = require('../parameter-information/parameters/energy-meter.json')
const { getParameterThresholds } = require('./parameter-thresholds')

let parameterInformationMap = {}

const loadParameterInformation = async ({ plants, devices = ['weather station', 'inverter', 'mfm', 'scb'] }) => {
  plants.forEach(plantId => {
    if (parameterInformationMap[plantId] === undefined ) parameterInformationMap[plantId] = {}
    
    devices.forEach(device => {
    if (parameterInformationMap[plantId][device] === undefined ) parameterInformationMap[plantId][device] = {}

      if (hasDevice({ plantId, device })) {
        parameterInformationMap[plantId][device] = getParameterThresholds({ plantId, device })
      }
    })
  })
  
}

const getStaticInfo = ({ device }) => {
  switch (device) {
    case 'inverter': return inverterParameters;
    case 'mfm': return mfmParameters;
    case 'weather station': return weatherStationParameters;
    case 'scb': return scbParameters 
    case 'energy meter': return emParameters
  }
}


const getParameter = ({ plantId, device, deviceId, parameter }) => {
  const deviceStaticInfo = getStaticInfo({ device })

  let info = {...( deviceStaticInfo && {...deviceStaticInfo[parameter]})}

  let thresholdInfo = null;
  if (parameterInformationMap[plantId] 
    && parameterInformationMap[plantId][device] 
    && parameterInformationMap[plantId][device][deviceId] ) {
      thresholdInfo = parameterInformationMap[plantId][device][deviceId][parameter]
    } {
      info = {...info, ...( thresholdInfo && {threshold: thresholdInfo})} 
    } 

    return info
    
}

module.exports = {
  loadParameterInformation,
  getParameter
}