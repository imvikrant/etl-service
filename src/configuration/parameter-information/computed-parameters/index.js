const inverterComputedParameterConfig = require('./inverter.json')
const mfmComputedParameterConfig = require('./mfm.json')
const emComputedParameterConfig = require('./energy-meter.json')
const wsComputedParameterConfig = require('./weather-station.json')


const getComputedParameterInfo = (device, parameter) => {
  switch(device) {
    case 'inverter'       : return inverterComputedParameterConfig[parameter] || null;
    case 'mfm'            : return mfmComputedParameterConfig[parameter] || null;
    case 'energy meter'   : return emComputedParameterConfig[parameter] || null;
    case 'weather station': return wsComputedParameterConfig[parameter] || null;
    default               : return null;
  }
}


const getComputedParameterList = (device) => {
  switch(device) {
    case 'inverter'       : return Object.keys( inverterComputedParameterConfig ) || null;
    case 'mfm'            : return Object.keys( mfmComputedParameterConfig ) || null;
    case 'energy meter'   : return Object.keys( emComputedParameterConfig ) || null;
    case 'weather station': return Object.keys( wsComputedParameterConfig ) || null;
    default               : return null;
  }
}

module.exports = {
  getComputedParameterInfo,
  getComputedParameterList
}