const { getEquipmentList, getEquipment } = require("../../device-information")

const fetchEmParameterThresholds = ({ plantId }) => {
  const emParameterThresholdMap = {}

  const emList = getEquipmentList({ plantId, device: 'energy meter' })

  const HYPOTHETICAL_MAX_ENERGY_PER_SEC = 1500 
  emList.forEach(id => {
    const em = getEquipment({plantId, device: 'energy meter', deviceId: id})
    const { dcCapacity } = em

    // Increase per second
    const energy  = dcCapacity && ((dcCapacity * 0.025) / 60)

    const thresholdMap = {}
    energy && (thresholdMap[`active_total_export_cumulative`] = { counter: { threshold: energy || HYPOTHETICAL_MAX_ENERGY_PER_SEC} }  )

    emParameterThresholdMap[id] = thresholdMap;
  })

  return emParameterThresholdMap
}

module.exports = {
  fetchEmParameterThresholds
}