const { getEquipmentList, getEquipment } = require("../../device-information")

const fetchMfmParameterThresholds = ({ plantId }) => {
  const mfmParameterThresholdMap = {}

  const mfmList = getEquipmentList({ plantId, device: 'mfm' })

  const HYPOTHETICAL_MAX_ENERGY_PER_SEC = 1500 
  mfmList.forEach(id => {
    const mfm = getEquipment({plantId, device: 'mfm', deviceId: id})
    const { dcCapacity } = mfm

    // Increase per second
    const energy  = dcCapacity && ((dcCapacity * 0.025) / 60)

    const thresholdMap = {}
    energy && (thresholdMap[`active_total_export_cumulative`] = { counter: { threshold: energy || HYPOTHETICAL_MAX_ENERGY_PER_SEC} }  )

    mfmParameterThresholdMap[id] = thresholdMap;
  })

  return mfmParameterThresholdMap
}


module.exports = {
  fetchMfmParameterThresholds
}