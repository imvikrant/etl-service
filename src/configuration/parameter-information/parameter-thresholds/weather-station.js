const { getEquipmentList, getEquipment } = require("../../device-information")

const fetchWeatherStationParameterThresholds = ({ plantId }) => {
  const weatherStationParameterThresholdMap = {}

  const weatherStationList = getEquipmentList({ plantId, device: 'weather station' })

  weatherStationList.forEach(id => {
    const weatherStation = getEquipment({plantId, device: 'weather station', deviceId: id})

    const thresholdMap = {}
    thresholdMap[`irradiance_vertical`]     = { range: { lowerThreshold: 0, upperThreshold: 1500 } }
    thresholdMap[`irradiance_horizontal`]   = { range: { lowerThreshold: 0, upperThreshold: 1500 } }
    weatherStationParameterThresholdMap[id] = thresholdMap;
  })

  return weatherStationParameterThresholdMap;
}

module.exports = {
  fetchWeatherStationParameterThresholds
}