const { fetchEmParameterThresholds } = require('./energy-meter');
const { fetchInverterParameterThresholds } = require('./inverter')
const { fetchMfmParameterThresholds } = require('./mfm');
const { fetchWeatherStationParameterThresholds } = require('./weather-station');

const getParameterThresholds = ({ plantId, device }) => {
  switch(device) {
    case 'inverter': return fetchInverterParameterThresholds({ plantId });
    case 'mfm': return fetchMfmParameterThresholds( { plantId});
    case 'weather station': return fetchWeatherStationParameterThresholds({ plantId })
    case 'energy meter': return fetchEmParameterThresholds({ plantId})
    default: return {}
  }
} 

module.exports = {
  getParameterThresholds
}