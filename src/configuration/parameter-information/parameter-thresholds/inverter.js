const { getEquipmentList, getEquipment } = require("../../device-information")

const fetchInverterParameterThresholds = ({ plantId }) => {
  const inverterParameterThresholdMap = {}

  const inverterList = getEquipmentList({ plantId, device: 'inverter' })

  // This is just a assumed upper limit value if config threshold is missing
  // to prevent the values from not being filtered and thus letting negative 
  // values to creep in.
  const HYPOTHETICAL_MAX_ENERGY_PER_SEC = 1500 

  inverterList.forEach(id => {
    const inverter = getEquipment({plantId, device: 'inverter', deviceId: id})
    const { type, acCapacity, dcCapacity } = inverter

    const acPower = acCapacity && (acCapacity * 1.2)
    const dcPower = dcCapacity && (dcCapacity * 1.2)
    const energy  = dcCapacity && ((type == 1 ? dcCapacity * 0.03 : dcCapacity * 0.067) / 60);// Increase per second

    const thresholdMap = {}
    energy && (thresholdMap[`energy`]   = { counter: { threshold: energy || HYPOTHETICAL_MAX_ENERGY_PER_SEC} })  
    acPower && (thresholdMap[`ac_power`] = { range: { lowerThreshold: 0, upperThreshold: acPower } })
    dcPower && (thresholdMap[`dc_power`] = { range: { lowerThreshold: 0, upperThreshold: dcPower } })

    inverterParameterThresholdMap[id] = thresholdMap;
  })

  return inverterParameterThresholdMap
}

module.exports = {
  fetchInverterParameterThresholds
}