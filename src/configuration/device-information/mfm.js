const { QueryTypes } = require('sequelize')
const { model } = require('../../utils/db')

const fetchMfmInformation = async ({ plants }) => {
  const sQuery = `
    SELECT 
      plant_id,
      device_id,
      ac_capacity ac_capacity,
      dc_capacity dc_capacity
    FROM
      tbl_if
    WHERE
      device_id IS NOT NULL
      ${plants ? `AND plant_id IN (${plants.join(', ')})` : ''}
  `

  const result = await model.query(sQuery, {
    type: QueryTypes.SELECT
  })

  const hashMap = {}

  result.forEach(data => {
    const { plant_id, device_id, ac_capacity, dc_capacity } = data;

    if (hashMap[plant_id] === undefined) {
      hashMap[plant_id] = {}
    }

    hashMap[plant_id][device_id] = {
      acCapacity: ac_capacity,
      dcCapacity: dc_capacity
    }
  })

  return hashMap;
}

module.exports = {
  fetchMfmInformation
}