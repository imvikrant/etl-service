const { fetchAnnunciator } = require("./annunciator")
const { fetchBreaker } = require("./breaker")
const { getPlantWiseReferenceDeviceList } = require("./device-reference-ids")
const { fetchEmInformation } = require("./energy-meter")
const { fetchInverterInformation } = require("./inverter")
const { fetchMfmInformation } = require("./mfm")
const { fetchMppt } = require("./mppt")
const { fetchRelay } = require("./relay")
const { fetchScb } = require("./scb")
const { fetchSoilSensor } = require("./soil-sensor")
const { fetchTracker } = require("./tracker")
const { fetchTransformer } = require("./transformer")
const { fetchUps } = require("./ups")
const { fetchWeatherStationInformation } = require("./weather-station")
const { fetchZmu } = require("./zmu")

let deviceReferenceIdMapping = {};
let deviceInformationMapping = {};

const loadDeviceInformation = async ( {plants} ) => {
  const inverterInformation = await fetchInverterInformation({ plants })
  const mfmInformation = await fetchMfmInformation({ plants })
  const weatherStationInformation = await fetchWeatherStationInformation({ plants })
  const emInformation = await fetchEmInformation({ plants})
  const annunciatorInformation = await fetchAnnunciator({ plants})
  const breakerInformation = await fetchBreaker({ plants})
  const mpptInformation = await fetchMppt({ plants})
  const relayInformation = await fetchRelay({ plants})
  const scbInformation = await fetchScb({ plants})
  const trackerInformation = await fetchTracker({ plants})
  const upsInformation = await fetchUps({ plants})
  const zmuInformation = await fetchZmu({ plants})
  const transformerInformation = await fetchTransformer({ plants})
  const soilSensorInformation = await fetchSoilSensor({ plants})


  deviceInformationMapping = {
    'inverter': inverterInformation,
    'mfm': mfmInformation,
    'weather station': weatherStationInformation,
    'energy meter': emInformation,
    'annunciator': annunciatorInformation,
    'breaker': breakerInformation,
    'mppt': mpptInformation,
    'relay': relayInformation,
    'scb': scbInformation,
    'tracker': trackerInformation,
    'ups': upsInformation,
    'zmu': zmuInformation,
    'transformer': transformerInformation,
    'soil sensor': soilSensorInformation
  }

  deviceReferenceIdMapping = await getPlantWiseReferenceDeviceList(plants) || {}
}

const getEquipment = ({ plantId, device, deviceId }) => {
  if (
    !deviceInformationMapping[device] 
    || !deviceInformationMapping[device][plantId] 
    || !deviceInformationMapping[device][plantId][deviceId]
  ) return;

  return deviceInformationMapping[device][plantId][deviceId];
}

const getEquipmentList = ({ plantId, device }) => {
  if (
    !deviceInformationMapping[device] 
    || !deviceInformationMapping[device][plantId] 
  ) return;

  return Object.keys(deviceInformationMapping[device][plantId]);
}

const getReferenceEquipmentList = ({ plantId, device }) => {
  if (
    !deviceReferenceIdMapping[plantId] 
    || !deviceReferenceIdMapping[plantId][device] 
  ) return;

  return deviceReferenceIdMapping[plantId][device]
}

const hasDevice = ({ plantId, device }) => {
  if (
    !deviceInformationMapping[device] 
    || !deviceInformationMapping[device][plantId] 
  ) return false;

  return true;
}

module.exports = {
  loadDeviceInformation,
  getEquipment,
  getEquipmentList,
  hasDevice,
  getReferenceEquipmentList,
}