const { QueryTypes } = require('sequelize')
const { model } = require('../../utils/db')

const fetchInverterInformation = async ({ plants }) => {
  const sQuery = `
    SELECT 
      plant_id,
      device_id,
      inverter_ac_capacity ac_capacity,
      inverter_dc_capacity dc_capacity,
      inverter_type type
    FROM
      tbl_inverters
    WHERE
      device_id IS NOT NULL
      ${plants ? `AND plant_id IN (${plants.join(', ')})` : ''}
  `

  const result = await model.query(sQuery, {
    type: QueryTypes.SELECT
  })

  const hashMap = {}

  result.forEach(data => {
    const { plant_id, device_id, ac_capacity, dc_capacity, type } = data;

    if (hashMap[plant_id] === undefined) {
      hashMap[plant_id] = {}
    }

    hashMap[plant_id][device_id] = {
      acCapacity: ac_capacity,
      dcCapacity: dc_capacity,
      type
    }
  })

  return hashMap;
}

module.exports = {
  fetchInverterInformation
}