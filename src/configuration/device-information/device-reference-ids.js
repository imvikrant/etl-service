const { QueryTypes } = require("sequelize")
const { model } = require("../../utils/db")

const getPlantWiseReferenceDeviceList = async (plants) => {
    const sQuery = `
      SELECT 
          plant_id, 
          CAST(IF(source = 'em', 'energy meter', source) AS CHAR) device, 
          ref_device_id device_list
      FROM
          tbl_generation_source
      WHERE
          LENGTH(ref_device_id) > 0
          ${plants && plants.length ? `AND plant_id IN (${plants})` : 'AND plant_id >= 100' } 
      GROUP BY plant_id , source
    `
  
    const result = await model.query(sQuery, {
      type: QueryTypes.SELECT
    })
  
    const mapPlantToDeviceList = {}
  
    result.forEach(({plant_id, device, device_list}) => {
      if (!device_list) return;

      if (!mapPlantToDeviceList[plant_id]) {
        mapPlantToDeviceList[plant_id] = {}
      }

      mapPlantToDeviceList[plant_id][device] = device_list.split(',')
    })
  
    return mapPlantToDeviceList
}

module.exports = {
  getPlantWiseReferenceDeviceList
}