const { QueryTypes } = require('sequelize')
const { model } = require('../../utils/db')

const fetchBreaker = async ({ plants }) => {
  const sQuery = `
    SELECT 
      plant_id,
      device_id
    FROM
      tbl_breaker
    WHERE
      device_id IS NOT NULL
      ${plants ? `AND plant_id IN (${plants.join(', ')})` : ''}
  `

  const result = await model.query(sQuery, {
    type: QueryTypes.SELECT
  })

  const hashMap = {}

  result.forEach(data => {
    const { plant_id, device_id } = data;

    if (hashMap[plant_id] === undefined) {
      hashMap[plant_id] = {}
    }

    hashMap[plant_id][device_id] = {}
  })

  return hashMap;
}

module.exports = {
  fetchBreaker
}