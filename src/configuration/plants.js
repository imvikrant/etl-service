const { QueryTypes } = require('sequelize')
const { model } = require('./../utils/db')

const getPlantList = async ({ timezone, includeOnlyActivePlants = false } = {}) => {
    const conditions = [
      ...(timezone ? [`timezone = '${timezone}'`] : []),
      ...(includeOnlyActivePlants ? [`plant_id >= 100`] : []),
    ]

    const sQuery = `
      SELECT plant_id FROM tbl_plant 
      ${ conditions.length ? `WHERE ${conditions.join(' AND ')}` : '' }
    `

    const result = await model.query(sQuery, {
        type: QueryTypes.SELECT
    })

    return result.map(({ plant_id }) => plant_id)
}


const getPlantWiseDeviceList = async (plants) => {

  const sQuery = `
    SELECT 
      plant_id,
      GROUP_CONCAT(LOWER(device_type)
          SEPARATOR ',') device_list
    FROM
      tbl_plant_device_report
      ${plants && plants.length ? `WHERE plant_id IN (${plants})` : 'WHERE plant_id >= 100' } 
    GROUP BY plant_id
  `

  const result = await model.query(sQuery, {
    type: QueryTypes.SELECT
  })

  const mapPlantToDeviceList = {}

  result.forEach(({plant_id, device_list}) => {
    device_list && (mapPlantToDeviceList[plant_id] = device_list.split(','))
  })

  return mapPlantToDeviceList
}

module.exports = {
  getPlantList,
  getPlantWiseDeviceList
}