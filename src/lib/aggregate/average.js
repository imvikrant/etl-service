const average = (tsData) => {
  const sum =  tsData
      .reduce((total, [,value]) => total + value, 0)

  return sum / tsData.length;
}

module.exports = {
  average
}