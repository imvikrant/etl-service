const { average } = require('./average')
const { sum } = require('./sum')
const { latest } = require('./latest')
const { increase } = require('./increase')
const { groupIntoWindows } = require('../utils/rollup-window')
const { runningDifference } = require('../utils/running-difference')

const getAggregateFn = (method) => {
  if (method == 'increase') return increase;
  if (method == 'average') return average;
  if (method == 'sum') return sum;
  if (method == 'latest') return latest;

  throw new Error('invalid aggregate method name')
}

const aggregateTs = (tsData, method, window) => {
  const id = tsData._id;
  const aggregateFn = getAggregateFn(method)

  if (aggregateFn === increase) {
    tsData = runningDifference(tsData)
  }

  const aggregateWindows = groupIntoWindows(tsData, window)

  const aggregatedData = aggregateWindows.map(tsWindowData => {
    const aggregateId = tsWindowData._aggregateId;
    const [ lowerBound, upperBound] = aggregateId.split('-')
    const aggregateTimestamp = parseInt(upperBound) + 1;
    const value = aggregateFn(tsWindowData)

    return [aggregateTimestamp, value]
  })

  aggregatedData._id = id
  return aggregatedData;
}

module.exports = {
  aggregateTs,
}