const { sum } = require("./sum");

const increase = (tsData) => {
  return sum(tsData)
}

module.exports = {
  increase
}