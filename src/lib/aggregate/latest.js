const latest = (tsData) => {
  return tsData[tsData.length - 1][1]
}

module.exports = {
  latest
}