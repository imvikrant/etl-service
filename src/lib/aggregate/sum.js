const sum = (tsData) => {
  return tsData
      .reduce((total, [, value]) => total + value, 0)
}

module.exports = {
  sum
}