const { latest } = require('../latest')

test('averages all values of timeseries', () => {
  const testData = [
    [1, 4],
    [2, 3],
    [3, 3],
    [4, 2],
  ]  

  expect(latest(testData)).toBe(2)
})