const {average} = require('../average')

test('averages all values of timeseries', () => {
  const testData = [
    [1, 4],
    [2, 6],
    [3, 4],
    [4, 5],
  ]  

  expect(average(testData)).toBeCloseTo(4.750)
})