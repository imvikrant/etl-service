const {sum} = require('../sum')

test('adds all values of timeseries', () => {
  const testData = [
    [1, 1],
    [2, 2],
    [3, 3],
    [4, 4],
  ]  

  expect(sum(testData)).toBe(10)
})