const { aggregateTs } = require("./aggregate")
const { filterTs } = require("./filter")
const { revertTimeSeries } = require("./utils/revert")
const { transformIntoTimeseries } = require("./utils/transform")

const filterTimeSeries = (tsDataPointCollection, filterOptions) => {
  const tsCollection = transformIntoTimeseries(tsDataPointCollection)

  const filteredData = tsCollection.map(tsData => {
    const parameter = tsData._id
    if (!filterOptions[parameter]) return tsData
    const config = filterOptions[parameter]
    return filterTs(tsData, config)
  }).filter(d => d !== null)

  return revertTimeSeries(filteredData)
}

const aggregateTimeSeries = (tsDataPointCollection, aggregateOptions, window) => {
  const tsCollection = transformIntoTimeseries(tsDataPointCollection)

  const filteredData = tsCollection.map(tsData => {
    const parameter = tsData._id
    if (!aggregateOptions[parameter]) return null
    const method = aggregateOptions[parameter].method
  
    return aggregateTs(tsData, method, window)
  }).filter(d => d !== null)

  return revertTimeSeries(filteredData)
}

module.exports = {
  filterTimeSeries,
  aggregateTimeSeries
}