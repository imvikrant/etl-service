const { filterCounter } = require("../counter")

/**
 * dataPoint format = [timestamp, value]
 */

test('returns the data unfiltered in case of no anamolies', () => {
  const tsData = [
    [ 60, 39 ],
    [ 120, 42 ],
    [ 180, 46 ],
    [ 240, 48 ],
    [ 300, 49 ],
  ] 

  const threshold = 4 / 60 //per second
  const filteredData = filterCounter(tsData, threshold)

  expect(filteredData).toEqual(tsData)
})


test('removes datapoints that are invalid', () => {
  const tsData = [
    [ 60, 39 ],
    [ 120, 42 ],
    [ 180, 46 ],
    [ 240, 4888 ], // removes this datapoint
    [ 300, 49 ],
  ] 

  const threshold = 4 / 60 //per second
  const filteredData = filterCounter(tsData, threshold)

  expect(filteredData).toEqual([
      [ 60, 39 ],
      [ 120, 42 ],
      [ 180, 46 ],
      [ 300, 49 ],
    ] 
  )
})


test('tags reset datapoints with a _isReset:true flag', () => {
  const tsData = [
    [ 60, 39 ],
    [ 120, 42 ],
    [ 180, 1 ],
    [ 240, 2 ], // removes this datapoint
    [ 300, 5 ],
  ] 

  const threshold = 4 / 60 //per second
  const filteredData = filterCounter(tsData, threshold)

  const expectedResult = [
    [ 60, 39 ],
    [ 120, 42 ],
    [ 180, 1 ],
    [ 240, 2 ], 
    [ 300, 5 ],
  ]
  expectedResult[2]._isReset = true;
  
  expect(filteredData).toEqual(expectedResult)
})



test('works for datapoints containing both junk and reset points', () => {
  const tsData = [
    [ 60, 39 ],
    [ 120, 42 ],
    [ 180, 65554 ], // junk
    [ 240, 65554 ], // junk  
    [ 360, 1 ], // reset
    [ 420, 2 ],
    [ 480, 3 ],
  ] 

  const threshold = 4 / 60 //per second
  const filteredData = filterCounter(tsData, threshold)

  const expectedResult = [
    [ 60, 39 ],
    [ 120, 42 ],
    [ 360, 1 ],
    [ 420, 2 ], 
    [ 480, 3 ],
  ]
  expectedResult[2]._isReset = true;
  
  expect(filteredData).toEqual(expectedResult)
})


