const { filterRange } = require("../range")

test('returns same data unfiltered if not anamolies', () => {
  const tsData = [
    [ 60, 39 ],
    [ 120, 42 ],
    [ 180, 46 ],
    [ 240, 48 ],
    [ 300, 49 ],
  ]
  const filtered = filterRange(tsData, 0, 50)

  expect(filtered).toEqual([...tsData])
})


test('removes junk values that fall outside given range', () => {
  const tsData = [
    [ 60, 39 ],
    [ 120, 42 ],
    [ 180, 1000 ],
    [ 240, 48 ],
    [ 300, -1111 ],
  ]
  const filtered = filterRange(tsData, 0, 50)

  const expectedResult = [
    [ 60, 39 ],
    [ 120, 42 ],
    [ 240, 48 ],
  ]
  
  expect(filtered).toEqual(expectedResult)
})
