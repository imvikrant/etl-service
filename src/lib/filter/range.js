const filterRange = (tsData, lowerThreshold, upperThreshold) => {
    return tsData.filter(([,value]) => {
        return value >= lowerThreshold && value <= upperThreshold;
    })
}

module.exports = {
    filterRange
}