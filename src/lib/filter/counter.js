const RESET   = 0
const ANAMOLY = -1
const VALID   = 1

const isValid = (currentDataPoint, lastValidPoint, threshold) => {
  const [timestamp, value]           = currentDataPoint
  const [validTimestamp, validValue] = lastValidPoint

  const timeInterval = timestamp - validTimestamp
  const difference   = value - validValue

  return difference > 0 && difference <= (threshold * timeInterval);
}


const analyseForAnamoly = (tsData, threshold) => {
  const filteredData   = [VALID]
  let   lastValidPoint = tsData[0];
  let   anamolyPoint   = null;

  for (let i = 1; i < tsData.length; i++) {
    const currentDataPoint = tsData[i]

    // Check For Reset Point
    if (anamolyPoint && !isValid(currentDataPoint, lastValidPoint, threshold) && isValid(currentDataPoint, anamolyPoint, threshold)) {
      filteredData.push(VALID)
      filteredData[i - 1] = RESET
      lastValidPoint = [...currentDataPoint];
      anamolyPoint = null;
      continue;
    }

    // Check for Data Anamoly
    if (!isValid(currentDataPoint, lastValidPoint, threshold)) {
      filteredData.push(ANAMOLY)
      anamolyPoint = [...currentDataPoint];
      continue;
    }

    filteredData.push(VALID)
    lastValidPoint = [...currentDataPoint]
    anamolyPoint = null;
  }

  return filteredData;
}


const removeAnamolies = (data, analysedVector) => {
  const finalData = []
  data.forEach((data, i) => {
    const d = data
    if (analysedVector[i] == VALID) { finalData.push(d) }
    if (analysedVector[i] == RESET) { d._isReset = true; finalData.push(d) }
  })

  return finalData;
}

const filterCounter = (tsData, threshold = 1000) => {
  const anomolyData = analyseForAnamoly(tsData, threshold)
  const cleanData   = removeAnamolies(tsData, anomolyData)

  return cleanData
}

module.exports = {
  filterCounter
}