/******************************************************************************************************
  Accepts the timeSeriesData to be filtered and the options on basis of which the filtration is done.

  Options should be provided in the format below:
    -  For cumulative data filter - { cumulativeFilter: { threshold: 3 }}, 
    -  For range data filter - {rangeFilter: { lowerThreshold, upperThreshold }}

*******************************************************************************************************/

const { filterRange } = require('./range')
const { filterCounter } = require('./counter')

const filterTs = (timeSeriesData, options) => {
    let result = []
    const id = timeSeriesData._id;

    if (options.counter && options.counter.threshold) {
        const threshold = options.counter.threshold
        result = filterCounter(timeSeriesData, threshold)
    }

    if (options.range) {
        const { lowerThreshold, upperThreshold } = options.range
        result = filterRange(timeSeriesData, lowerThreshold, upperThreshold)
    }

    // Find a better way to do this
    result._id = id
    return result
}

module.exports = { filterTs }