const groupIntoWindows = (tsData, window) => {
  const groupedByWindow = {}

  tsData.forEach(([timestamp, value]) => {
      const lowerBound = Math.floor(timestamp / window) * window
      const upperBound = lowerBound + window - 1;
      const windowHash = `${lowerBound}-${upperBound}`

      if (!groupedByWindow[windowHash]) {
          groupedByWindow[windowHash] = []
      }

      groupedByWindow[windowHash].push([timestamp, value])
  })

  return Object.entries(groupedByWindow).map(([hashId, tsWindowData]) => {
    tsWindowData._aggregateId = hashId
    return tsWindowData; 
  })
}

module.exports = {
  groupIntoWindows
}