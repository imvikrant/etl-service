const { groupIntoWindows } = require("../rollup-window")

test('groups into time windows', () => {
  const tsData = [
    [60,  1 ],
    [120,  3],
    [280,  5],
    [360,  1],
    [420,  3],
    [480,  5],
  ]

  const windowedData = groupIntoWindows(tsData, 300)
  
  expect(JSON.stringify(windowedData)).toEqual(JSON.stringify([
    [
      [60,  1 ],
      [120,  3],
      [280,  5],
    ],
    [
      [360,  1],
      [420,  3],
      [480,  5],
    ]
  ]))
})