const { revertTimeSeries } = require("../revert")

const tsData1= [
  [60, 1],
  [120, 3],
  [180, 5],
]
tsData1._id = 'ac_power'
tsData1[1]._isReset = true;

const tsData2= [
  [60, 5],
  [120, 10],
  [180, 15],
]
tsData2._id = 'energy'

const tsData3= [
  [60, 1],
  [120, 3],
  [180, 5],
]
tsData3._id = 'ac_frequency'

const tsCollection = [
  tsData1, tsData2
]

test('converts into timeseries data', () => {
  const timeseries = revertTimeSeries(tsCollection)
  
  expect(timeseries).toEqual([
    { timestamp: 60, ac_power: 1, energy: 5 },
    { timestamp: 120, ac_power: 3, energy: 10 },
    { timestamp: 180, ac_power: 5, energy: 15 },
  ])
})

test('adds metadata for only reset timestamps', () => {
  const timeseries = revertTimeSeries(tsCollection)

  expect(timeseries[1].metaData).toEqual({ac_power: { _isReset: true }})
  expect(timeseries[2].metaData).toEqual({})
})


test('metaData is empty object if no reset datapoints', () => {
  const tsColl = [tsData2, tsData3]
  const timeseries = revertTimeSeries(tsColl)

  expect(timeseries[1].metaData).toEqual({})
})

