const { transformIntoTimeseries } = require("../transform")

const timeseries1 = [
  { timestamp: 60, ac_power: 1, energy: 5 },
  { timestamp: 120, ac_power: 3, energy: 10 },
  { timestamp: 180, ac_power: 5, energy: 15 },
]


test('outputs datapoint collection', () => {
  const tsCollection = transformIntoTimeseries(timeseries1)

  const tsData1= [
    [60, 1],
    [120, 3],
    [180, 5],
  ]
  tsData1._id = 'ac_power'

  const tsData2= [
    [60, 5],
    [120, 10],
    [180, 15],
  ]
  tsData2._id = 'energy'

  expect(tsCollection).toEqual([
    tsData1, tsData2
  ])
})


const timeseries2 = [
  { timestamp: 60, ac_power: 1, energy: 5 },
  { timestamp: 120, ac_power: 3, energy: 10 },
  { timestamp: 180, ac_power: 5, energy: 15 },
]
Object.defineProperty(timeseries2[1], 'metaData', {value: {
  energy: {
    _isReset: true
  }
}})

test('flags reset datapoint as _isReset:true', () => {
  const tsCollection = transformIntoTimeseries(timeseries2)

  const energyTsData = tsCollection.find(d => d._id === 'energy')

  expect(energyTsData[1]).toHaveProperty('_isReset');
})

test('doest not flag datapoint as _isReset:true if not defined in metaData', () => {
  const tsCollection = transformIntoTimeseries(timeseries2)

  const energyTsData = tsCollection.find(d => d._id === 'ac_power')

  expect(energyTsData[1]).not.toHaveProperty('_isReset');
})
