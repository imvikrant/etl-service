const runningDifference = (tsData, dropHead = true, ignoreReset = false ) => {
  const id = tsData._id;
  const runningData = tsData.map((dataPoint, i) => {
    if (!tsData[i - 1] || dataPoint._isReset) { return [ dataPoint[0], 0 ] }
    
    const [, prevValue] = tsData[i - 1]
    const [timestamp, value] = dataPoint
    const difference = value - prevValue;

    return [ timestamp, difference]
  })
  
  if (dropHead) { runningData.shift() }

  runningData._id = id;
  return runningData
}

module.exports = {
  runningDifference
}