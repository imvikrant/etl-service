const revertTimeSeries = (tsCollection) => {
  const merged = {}

  tsCollection.forEach(tsData => {
    const parameter = tsData._id

    tsData.forEach((point) => {
      const [timestamp, value] = point
      const isReset = !!point._isReset

      if (merged[timestamp] === undefined) {
        merged[timestamp] = { timestamp }
        Object.defineProperty(merged[timestamp], 'metaData', { value: {} })
      }

      merged[timestamp][parameter] = value
      if (isReset) {
        merged[timestamp]['metaData'][parameter] = { _isReset: true }
      }
    })
  })

  return Object.values(merged);
}

module.exports = {
  revertTimeSeries
}