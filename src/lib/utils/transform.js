// Returns array of grouped timeseries data. THis is group is an array of data poinst
// TODO: extract out some hardcoded valeus into arguments
const transformIntoTimeseries = (dataPointCollection) => {
  const extractedData = {}

  dataPointCollection.forEach(dataPoint => {
    const { timestamp, metaData, ...valueFields } = dataPoint
    
    if (!timestamp) return;
    
    Object.entries(valueFields).forEach(([field, value]) => {
      const id = `${field}`
      
      if (value === null || value === undefined) return

      if (!extractedData[id]) {
        extractedData[id] = []
      }

      const point = [timestamp, value]
      
      if (metaData && metaData[field] && metaData[field]._isReset) {
        point._isReset = true
      }

      extractedData[id].push(point)
    })
  })

  return Object.entries(extractedData).map(([id, timeseries]) => {
    timeseries._id = id;
    return timeseries
  })
}

module.exports = {
  transformIntoTimeseries
}