const Redis =  require('ioredis')
const config =  require('./config')

const createRedisClient = () => {
  const redisEndpoint = config.redis.endPoint;
  return new Redis(redisEndpoint,  {
    lazyConnect: true // disables auto connect
  });
}

module.exports = {
  createRedisClient
}