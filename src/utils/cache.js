const { chunk } = require('lodash')

const makeRemoteCache = (client) => {
  const setMap = async (hashKey, map) => {
    const valuesToCache = Object.entries(map).map(([key, value]) => {
      return [key, JSON.stringify(value)] 
    })

    if (!valuesToCache || !valuesToCache.length) return;

    const batches = chunk(valuesToCache, 50)
    const pipeline = client.pipeline()

    batches.forEach(batch => {
      pipeline.hset(hashKey, ...batch.flat())
    })

    const result = await pipeline.exec()
    console.log(result) 
  }

  const getMap = async (hashKey) => {
    const parsedMap = {}
    const hashMap   = await client.hgetall(hashKey)

    Object.entries(hashMap).forEach(([key, value]) => {
      parsedMap[key] = JSON.parse(value)
    })

    return parsedMap;
  }

  return {
    getMap,
    setMap
  }
}

module.exports = {
  makeRemoteCache
}