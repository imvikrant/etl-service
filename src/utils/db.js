const Sequelize = require('sequelize')
const config = require('./config')

const { database, username, password, host, readHost, writeHost, dialect } = config.db

const createSqlClient = () => {
  const connectionOptions = {
    database,
    dialect,
    timezone: '+05:30',
    logging : false,
    retry   : 5,
    pool    : {
      max: 20,
      min: 0
    },
    ...(
      host && { username, password, host }
    ),
    ...(
      readHost && writeHost && 
      {
        replication: {
          read : { username, password, host: readHost },
          write: { username, password, host: writeHost },
        }
      }
    ),
  }

  return new Sequelize(connectionOptions)
}

const model = createSqlClient()

module.exports = { model }