const { makeRemoteCache } = require("./cache");
const { createRedisClient } = require("./redis");

let localCache = null;
let remoteCache = null;

const set = (key, value) => {
  localCache[key] = value
}

const get = (key) => {
  return localCache[key]
}

const initialize = async ({ remote = false } = {}) => {
  if (localCache !== null) return;
  localCache = {}
  
  if (remote && remoteCache === null) {
    const redisClient = createRedisClient()

    await redisClient.connect()
    console.log(`[Remote Cache] Connected!`)

    remoteCache = makeRemoteCache(redisClient)
    localCache = await remoteCache.getMap('_aggregate')
    console.log(`[Remote Cache] Synced!`)
  }

  console.log('[Cache] Initialized!')
}

const syncToRemote = async () => {
  if (!remoteCache) throw new Error('Remote not enabled')

  await remoteCache.setMap('_aggregate', localCache)
  console.log('[Cache] Synced to remote!') 
}

module.exports = {
  set,
  get,
  syncToRemote,
  initialize
}