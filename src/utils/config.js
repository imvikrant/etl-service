const dotenv =  require('dotenv');
const path =  require('path')

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envPath = null;

switch (process.env.NODE_ENV) {
  case 'development': path.resolve(process.cwd(), '.env'); break;
  case 'production' : path.resolve(process.cwd(), '.env.production'); break;
  case 'test'       : path.resolve(process.cwd(), '.env.test'); break;
}

const envFound = dotenv.config({
  path: envPath
});

if (envFound.error) {
  throw new Error(".env file Not Found");
}

module.exports = {
  db: {
    database : process.env.DB_NAME,
    username : process.env.DB_USERNAME,
    password : process.env.DB_PASSWORD,
    host     : process.env.DB_HOST,
    readHost : process.env.DB_READ_HOST,
    writeHost: process.env.DB_WRITE_HOST,
    dialect  : 'mysql'
  },
  devDb: {
    database : process.env.DEV_DB_NAME,
    username : process.env.DEV_DB_USERNAME,
    password : process.env.DEV_DB_PASSWORD,
    host     : process.env.DEV_DB_HOST,
    dialect  : 'mysql'
  },
  redis: {
    endPoint : process.env.REDIS_ENDPOINT
  }
}