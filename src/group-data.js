const groupSensorData = (sensorData) => {
  const groupedSensorData = {}

  sensorData.forEach(dataPoint => {
    const {
      timestamp,
      device,
      plant_id: plantId,
      device_id: deviceId,
      ...parameterCollection
    } = dataPoint;

    const _id = [plantId, device, deviceId].join('#')
    const condensed = { timestamp, ...parameterCollection }

    if (!groupedSensorData[_id]) {
      groupedSensorData[_id] = { _id, plantId, device, deviceId, parameters: Object.keys(parameterCollection), data: [] }
    }
    groupedSensorData[_id].data.push(condensed)
  })

  return Object.values(groupedSensorData)
}

async function* generateByGroup(generator) {
  for await (const sensorData of generator) {
    const groupedCollection = groupSensorData(sensorData)
    for (const collection of groupedCollection) {
      yield collection
    }
  }
}

module.exports = {
  generateByGroup
}