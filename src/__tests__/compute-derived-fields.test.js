const { computeDerivedFields } = require("../compute-derived-fields")

test('outputs derived field and value if all dependent fields are available', () => {
  const testDataPoint = { timestamp: 60, packet_count: 5, irradiance_vertical: 300, radiation: 100 }

  const derivedDataPoint = computeDerivedFields(testDataPoint, ['poa_insolation'])

  expect(derivedDataPoint).toHaveProperty('poa_insolation')
})


test('returns null if dependent field(s) are not available in the dataPoint', () => {
  const testDataPoint = { timestamp: 60, packet_count: 5, radiation: 100 }

  const derivedDataPoint = computeDerivedFields(testDataPoint, ['poa_insolation'])

  expect(derivedDataPoint).toBeNull()
})


test('works for first multiple argument name separated by |', () => {
  const testDataPoint = { timestamp: 60, packet_count: 5, energy: 100 }

  const derivedDataPoint = computeDerivedFields(testDataPoint, ['ac_yield'], { dcCapacity: 50 })

  expect(derivedDataPoint).not.toBeNull()
  expect(derivedDataPoint).toHaveProperty('ac_yield')
})


test('works for second multiple argument separated by |', () => {
  const testDataPoint = { timestamp: 60, packet_count: 5, active_total_export_cumulative: 1000 }

  const derivedDataPoint = computeDerivedFields(testDataPoint, ['ac_yield'], { dcCapacity: 50 })

  expect(derivedDataPoint).not.toBeNull()
  expect(derivedDataPoint).toHaveProperty('ac_yield')
})

test('returns null if a meta variable is not available', () => {
  const testDataPoint = { timestamp: 60, packet_count: 5, active_total_export_cumulative: 1000 }

  const derivedDataPoint = computeDerivedFields(testDataPoint, ['ac_yield'], {})

  expect(derivedDataPoint).toBeNull()
})