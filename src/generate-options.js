const { getParameter } = require("./configuration")
const { getParameterInfo } = require("./configuration/parameter-information/parameters")

const generateFilterOptions = ({plantId, device, deviceId, parameters}) => {
  const filterOptions    = {}

  parameters.forEach(parameter => {
    const paramName = parameter.split('-')[0]
    const parameterInfo = getParameter({ plantId, device, deviceId, parameter: paramName })

    parameterInfo && parameterInfo.threshold && (filterOptions[parameter] = parameterInfo.threshold);
  })

  return filterOptions;
}

const generateAggregateOptions = ({plantId, device, deviceId, parameters}) => {
  const aggregateOptions    = {}

  parameters.forEach(parameter => {
    const paramName = parameter.split('-')[0]
    const parameterInfo = getParameterInfo(device, paramName)

    parameterInfo && parameterInfo.aggregateMethod && (aggregateOptions[parameter] = { method: parameterInfo.aggregateMethod }); 
  })

  return aggregateOptions;
}

module.exports = {
  generateFilterOptions,
  generateAggregateOptions
}