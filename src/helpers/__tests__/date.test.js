const { Datetime, getUnixtime, getTimezone, startOfMinute, subtractMinutes, startOfDay, subtractDays } = require('../date.js')

test('getUnixtime: outputs unix time correctly', () => {
  const timestamp = new Date(2000, 0, 1, 12, 0, 0)
  const datetime = Datetime(timestamp)

  const unixtime = getUnixtime(datetime)
  expect(unixtime).toBe(946708200)
})

test('getTimeZone: outputs correct timezone', () => {
  const timestamp = new Date(2000, 0, 1, 12, 0, 0)
  const timezone = 'Asia/Kolkata'
  const datetime = Datetime(timestamp, timezone)

  const tz = getTimezone(datetime)
  expect(tz).toBe(timezone)
})

test('startOfMinute: outputs correct start of minute', () => {
  const timestamp = new Date(2000, 0, 1, 12, 0, 30)
  const datetime = Datetime(timestamp)

  const unixtime = getUnixtime(startOfMinute(datetime))
  expect(unixtime).toBe(946708200)
})

test('subtractMinute: subtracts correctly', () => {
  const timestamp = new Date(2000, 0, 7, 12, 0, 0)
  const datetime = Datetime(timestamp)

  const unixtime = getUnixtime(subtractDays(datetime, 3))
  expect(unixtime).toBe(946967400)
})

test('subtractMinute: subtracts correctly', () => {
  const timestamp = new Date(2000, 0, 1, 12, 0, 0)
  const datetime = Datetime(timestamp)

  const unixtime = getUnixtime(subtractMinutes(datetime, 30))
  expect(unixtime).toBe(946706400)
})

test('startOfDay: outputs correct start of day based on timezone', () => {
  const timestamp = new Date(2000, 0, 2, 12, 0, 0)
  const datetime1 = Datetime(timestamp, 'Asia/Kolkata')
  const datetime2 = Datetime(timestamp, 'Africa/Kigali')

  const unixtime1 = getUnixtime(startOfDay(datetime1))
  const unixtime2 = getUnixtime(startOfDay(datetime2))

  expect(unixtime1).toBe(946751400)
  expect(unixtime2).toBe(946764000)
})