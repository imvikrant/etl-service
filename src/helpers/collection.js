const mergeCollection = (collection, mergeKey) => {
  const merged = {}

  collection.forEach(data => {
    const mergeValue = data[mergeKey]

    if (merged[mergeValue] === undefined) {
      return (merged[mergeValue] = data)
    }

    merged[mergeValue] = { ...merged[mergeValue], ...data }
  })

  return Object.values(merged);
}

module.exports = {
  mergeCollection
}