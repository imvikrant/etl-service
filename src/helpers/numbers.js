// Parse as Number: Output range [-Infinity, +Infinity] & null.
const parseNumber = (number) => {
  const parsedValue = parseFloat(number)
  return isNaN(parsedValue) ? null: parsedValue
}

// Parse each value in a csv string as numeric array. 
const parseNumericArray = (csv, separator = ',') => {
  return csv.split(separator).map(strNum => {
    const value = parseFloat(strNum);
    
    return isNaN(value) ? null: value;
  })
}

const isNumber = (value) => {
  const parsedNumber = parseFloat(value)
  return isFinite(parsedNumber)
}

module.exports = {
  parseNumber,
  parseNumericArray,
  isNumber
}