const spacetime = require('spacetime')

const msToSeconds = (timestamp) => {
  return Math.floor(timestamp / 1000)
}

const Datetime = (timestamp, timezone) => {
  if (Number.isFinite(timestamp)) {
    timestamp = timestamp * 1000 
  }

  const s = spacetime(timestamp, timezone)

  s.unix = () => msToSeconds(s.epoch)
 
  return s;
}

const now = (timezone) => {
  return Datetime(null, timezone)
}

const getUnixtime = (dt) => {
  return dt.unix()
}

const getTimezone = (dt) => {
  return dt.timezone().name
}

const startOfMinute = (dt) => {
  return Datetime(dt.startOf('minute'))
}

const startOfDay = (dt) => {
  return Datetime(dt.startOf('day'))
}

const endOfDay = (dt) => {
  return Datetime(dt.endOf('day'))
}

const startOfMonth = (dt) => {
  return Datetime(dt.startOf('month'))
}

const endOfMonth = (dt) => {
  return Datetime(dt.endOf('month'))
}

const startOfYear = (dt) => {
  return Datetime(dt.startOf('year'))
}

const endOfYear = (dt) => {
  return Datetime(dt.endOf('year'))
}

const subtractDays = (dt, interval) => {
  return Datetime(dt.subtract(interval, 'day'))
}

const subtractMinutes = (dt, interval) => {
  return Datetime(dt.subtract(interval, 'minute'))
}

const addSecond = (dt, interval) => {
  return Datetime(dt.add(interval, 'second'))
}

const add = (dt, interval, unit) => {
  return Datetime(dt.add(interval, unit));
}

const isAfter = (dt1, dt2, interval) => {
  return dt1.isAfter(dt2, interval)
}

const isBefore = (dt1, dt2, interval) => {
  return dt1.isBefore(dt2, interval)
}

const diff = (dt1, dt2, interval) => {
  return dt2.diff(dt1, interval)
}

const clone = (dt) => {
  return Datetime(dt.clone())
}

const min = (dt1, dt2) => {
  if (dt1.isBefore(dt2)) return Datetime(dt1);

  return Datetime(dt2)
}

const max = (dt1, dt2) => {
  if (dt1.isAfter(dt2)) return Datetime(dt1);

  return Datetime(dt2)
}

const startOf = (dt, unit) => {
  return Datetime(dt.startOf(unit))
}

const endOf = (dt, unit) => {
  return Datetime(dt.endOf(unit))
}

module.exports = {
  Datetime,
  now,
  getUnixtime,
  getTimezone,
  startOfMinute,
  startOfDay,
  endOfDay,
  subtractDays,
  subtractMinutes,
  startOfMonth,
  endOfMonth,
  startOfYear,
  endOfYear,
  isAfter,
  isBefore,
  addSecond,
  diff,
  clone,
  min,
  max,
  add,
  startOf,
  endOf,
}