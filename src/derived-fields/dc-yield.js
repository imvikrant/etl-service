const dcYield = (energy, dcCapacity) => {
  return (energy * 1.03) / dcCapacity
}

module.exports = {
  dcYield
}