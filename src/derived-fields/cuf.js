const cuf = (energy, dcCapacity) => {
  return (energy / dcCapacity) * (100 / 24)
}

module.exports = {
  cuf
}