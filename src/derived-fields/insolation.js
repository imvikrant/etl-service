const insolation = (avgIrr, packetCount) => {
  if (avgIrr == 0) return 0;
  if (!avgIrr) return null;

  // insolation = (Total Irradiance / packetCount) * (5 / 60) / 1000
  // insolation = Average Irradiance * 5 / 60 / 1000
  const K = 0.00008333333 // (5 / 60) / 1000

  return avgIrr * K
} 

module.exports = {
  insolation
}