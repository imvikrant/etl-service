const { insolation } = require('./insolation')
const { acYield } = require('./ac-yield')
const { dcYield } = require('./dc-yield')
const { cuf } = require('./cuf')

module.exports = {
  insolation,
  acYield,
  dcYield,
  cuf
}