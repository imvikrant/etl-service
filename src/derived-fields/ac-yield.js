const acYield = (energy, dcCapacity) => {
  return energy / dcCapacity
}

module.exports = {
  acYield
}