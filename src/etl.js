const _ = require('lodash')
const localStore = require("./utils/local-cache");
const { getParameter, loadConfiguration, getEquipment } = require('./configuration');
const { mergeCollection } = require('./helpers/collection');
const { filterTimeSeries, aggregateTimeSeries } = require("./lib");
const { generateSensorData } = require('./data-repository/index.js');
const { loadData } = require("./load-data");
const { generateByGroup } = require('./group-data');
const { generateFilterOptions, generateAggregateOptions } = require("./generate-options");
const { computeDerivedData } = require('./compute-derived-fields');
const { isEmpty } = require('lodash');
const { getParameterList } = require('./configuration/parameter-information/parameters');

// In seconds
const FIVE_MINUTES = 5 * 60

const deviceToComputedFieldsMapping = {
  'weather station': ['poa_insolation', 'ghi_insolation'],
  'inverter'       : ['ac_yield', 'dc_yield', 'cuf'],
  'mfm'            : ['ac_yield', 'cuf'],
  'energy meter'   : ['ac_yield', 'cuf']
}

const cacheCounterData = (sensorData) => {
  const { plantId, device, deviceId, _id, parameters, data } = sensorData;

  parameters.forEach(parameter => {
    const parameterInfo = getParameter({ plantId, device, deviceId, parameter })
    if (!parameterInfo || parameterInfo.valueType != 'counter') return;

    const latestValidData = _.findLast(data, dataPoint => !_.isNil(dataPoint[parameter]));

    if (!latestValidData) return;

    const key = `${_id}#${parameter}`
    const value = { value: latestValidData[parameter], timestamp: latestValidData.timestamp }

    localStore.set(key, value)
  })
}


const appendCachedData = (sensorData) => {
  const { _id, parameters, data } = sensorData
  const cachedData = []

  parameters.forEach((parameter) => {
    const valueObj = localStore.get(`${_id}#${parameter}`)
    if (!valueObj) return;

    const { timestamp, value } = valueObj

    cachedData.push({ timestamp, [parameter]: value })
  })

  const mergedCollection = mergeCollection(cachedData, 'timestamp')
  data.unshift(...mergedCollection)

  return sensorData
}

const sanitizeQueryList = (queryList) => {
  return queryList.map(query => {
    const { device, fields } = query 
    
    const fieldList = getParameterList(device) || []
    
    return ({
      ...query,
      fields: fields || fieldList 
    })
  })
}

async function* appendCacheTransformer(sensorDataGenerator) {
  for await (let sensorData of sensorDataGenerator) {
    yield appendCachedData(sensorData)
  }
}

// add new parameter name/value pairs for aggregation
const addExtraAggregateParameters = (device, data) => {
  const acceptedDeviceSet = new Set(['inverter', 'energy meter', 'mfm'])
  if (!device || !acceptedDeviceSet.has(device)) return data;

  return newData = data.map(dataPoint => {
    const newParameters = {}
    dataPoint.energy && (newParameters.cumulative_energy = dataPoint.energy);
    dataPoint.active_total_export_cumulative && (newParameters.cumulative_active_total_export_cumulative = dataPoint.active_total_export_cumulative);
    
    return { ...dataPoint, ...newParameters }
  })
}

const etl = async (queryList, { 
  rollUpWindow   = FIVE_MINUTES,
  readBatchSize  = 1000,
  writeConcurrency = 1,
  writeBatchSize = 1000,
  cacheRemote    = false,
} = {}) => {
  let readRows = 0;
  let writeRows = 0;
  await loadConfiguration({ plants: queryList.map(({ plantId }) => plantId) })
  const sanitizedQueryList = sanitizeQueryList(queryList)
  await localStore.initialize({ remote: !!cacheRemote })
  const sensorDataGenerator = appendCacheTransformer(
    generateByGroup(
      generateSensorData(sanitizedQueryList, { batchSize: readBatchSize })
    )
  )

  for await (const sensorData of sensorDataGenerator) {
    if (!sensorData || !sensorData.data || !sensorData.data.length) continue;

    const {  data, ...metaInfo  } = sensorData
    const { plantId, device, deviceId, _id, parameters } = metaInfo
    let derivedData = []

    readRows+= data.length

    const equipmentMetaInfo = getEquipment({plantId, device, deviceId })

    if (!isEmpty(equipmentMetaInfo)) {
      Object.assign(metaInfo, equipmentMetaInfo)
    }
    
    data.forEach(d => d.packet_count = 1)

    const filterOptions = generateFilterOptions({ plantId, device, deviceId, parameters })
    const aggregateOptions = generateAggregateOptions({ plantId, device, deviceId, parameters })

    const filteredData = filterTimeSeries(data, filterOptions)
    cacheCounterData({ plantId, device, deviceId, _id, parameters, data: filteredData })

    const aggregatedData = aggregateTimeSeries(addExtraAggregateParameters(device, filteredData), {
      ...aggregateOptions, 
      packet_count: { method: 'sum' },
      cumulative_energy: { method: 'latest' },
      cumulative_active_total_export_cumulative: { method: 'latest'}
    }, rollUpWindow)

    if (deviceToComputedFieldsMapping[device]) {
      derivedData = computeDerivedData(aggregatedData, deviceToComputedFieldsMapping[device], metaInfo)
    }

    const rowCount = await loadData({ plantId, device, deviceId, _id, parameters, data: [...aggregatedData, ...derivedData] }, {bufferSize: writeBatchSize, concurrency: writeConcurrency})
    writeRows += rowCount

  }

  const rowCount = await loadData(null, { bufferSize: writeBatchSize, concurrency: writeConcurrency })
  writeRows += rowCount
  cacheRemote && await localStore.syncToRemote()

  console.log('Device aggregate successful')

  return ({
    rowsRead: readRows,
    rowsWritten: writeRows
  })

}

module.exports = {
  etl
}