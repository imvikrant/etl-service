const { QueryTypes } = require('sequelize')
const { model } = require('../utils/db')
const _ = require('lodash')

const parseNumber = (value) => {
  const num = parseFloat(value)
  return isNaN(num) ? null : num
}

const insertAggregateData = async (data) => {
  if (!data || !data.length) return;

  const chunkedData = _.chunk(data, 500)

  for (let d of chunkedData) {
    const values = d.map(
      ({ timestamp, plantId, device, deviceId, parameter, value }) => 
        `(${timestamp}, ${plantId}, '${device}', '${deviceId}', '${parameter}', ${parseNumber(value)})`
    )

    const sQuery = `
      INSERT INTO 
        tbl_utc_device_aggregate_data(sensor_time, plant_id, device, device_id, parameter, value)
      VALUES 
        ${values.join(`,\n`)}
      ON DUPLICATE KEY 
        UPDATE
          value = VALUES(value)
    `
    await model.query(sQuery, {
      type: QueryTypes.INSERT
    })
  }
}

module.exports = {
  insertAggregateData
}