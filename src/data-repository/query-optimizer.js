/**
 * !IMPORTANT
 * This module optimizes a list of individual DB read querys, such that data fetched from
 * the DB is close to the provided 'rowCount'. If the query is expected to fetch more rows than
 * provided row count then it splits it up, on the other hand if there are bunch of small reads,
 * it clubs up into a single read query which can be read through UNIONS in SQL query at one shot,
 * instead of multiple read queries. 
 * 
 * The expected row count is based on the number of equipmentIds and the timeRange.
 * For each equipment Id and a time interval of 1 minute, the expected row count is 1. Thus the 
 * expected row count is factor of number of equipment Ids and the time interval.
 * 
 * The reason for doing this instead of using a SQL LIMIT statement is, we want the data to be fetched
 * for the entire interval of multiples of 5minute (or any general time window). Using limit doesnt 
 * garantee this, which is vital for aggregating the data.
 * 
 * Also the grouping is done based on device level, since each device has a separate data table, and it
 * would not be possible to do a UNION of different tables with separate schema 
 *
 */

const { groupBy } = require("lodash")

// TODO: Refactor
const optimizeQueryList = (queryData, rowCount = 1000) => {
  const groups = groupBy(queryData, 'device')

  const optimizedQueryList = Object.values(groups).flatMap(groupedData => {
    const splitQueries = []

    groupedData.forEach((query) => {
      const { equipmentIds, timeRange, ...queryData } = query

      equipmentIds.forEach(equipmentId => {
        const startTimestamp = Math.floor(timeRange[0] / 300) * 300;
        const endTimestamp   = Math.ceil(timeRange[1] / 300) * 300;
        const interval       = Math.ceil((endTimestamp - startTimestamp) / 60)

        const optimizerMetaData = {
            startTimestamp,
            endTimestamp,
            interval,
          }

        splitQueries.push({
          equipmentId,
          ...queryData,
          optimizerMetaData 
        })
      })
    })


    const bucketLimit = rowCount
    const buckets = [[]]


    let i = 0;
    let currentBucket = buckets[0]
    currentBucket.size = 0

    while (i < splitQueries.length) {

      const { optimizerMetaData, ...queryData } = splitQueries[i]
      const { startTimestamp, endTimestamp, interval } = optimizerMetaData

      let availableLimit = bucketLimit - currentBucket.size;

      if (availableLimit <= 0) {
        buckets.push([])
        currentBucket = buckets[buckets.length - 1]
        currentBucket.size = 0;
        availableLimit = bucketLimit
      }

      if (interval <= availableLimit) {
        currentBucket.push({ ...queryData, timeRange: [startTimestamp, endTimestamp] })
        currentBucket.size = currentBucket.size + interval
        i++;
      }

      if (interval > availableLimit) {
        const overflowInterval = interval - availableLimit;
        optimizerMetaData.interval = overflowInterval;
        currentBucket.push({ ...queryData, timeRange: [startTimestamp, (availableLimit * 60) + optimizerMetaData.startTimestamp] })
        optimizerMetaData.startTimestamp = (availableLimit * 60) + optimizerMetaData.startTimestamp;
        currentBucket.size = currentBucket.size + availableLimit
      }
    }

    return buckets;
  })

  return optimizedQueryList.map(bucketQueryList => {
    const grouping = {}

    bucketQueryList.forEach(query => {
      const { plantId, timeRange, equipmentId, device, fields } = query
      const id = `${plantId}-${timeRange}`;

      if (!grouping[id]) {
        grouping[id] = { device, plantId, timeRange, equipmentIds: [], fields }
      }

      grouping[id].equipmentIds.push(equipmentId)
    })

    return Object.values(grouping)
  })
}


module.exports = {
  optimizeQueryList
}
