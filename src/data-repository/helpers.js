const arrayToObject = (valueArray, prependString) => {
  if (!Array.isArray(valueArray)) return null;

  const expandedArray = valueArray.map((value, i) => [`${prependString}-${i + 1}`, value])

  return Object.fromEntries(expandedArray)
}

const batchQuery = (queryParameter, batchInverval) => {
  const [ startUnixTime, endUnixTime ] = queryParameter.timeRange
  const interval =  endUnixTime - startUnixTime

  if (interval < 0) throw Error('[Data Generator] Time Range is Invalid')

  if (interval <= batchInverval) return [queryParameter]

  const batches = []
  let batchUnixTime = startUnixTime
  
  do {
    const startBatchUnixTime = batchUnixTime  
    const endBatchUnixTime = Math.min(batchUnixTime + batchInverval, endUnixTime)

    batches.push([ startBatchUnixTime, endBatchUnixTime ])
    batchUnixTime = endBatchUnixTime + 1
  } 
  while (batchUnixTime < endUnixTime)

  return batches.map(batchTimeRange => ({ ...queryParameter, timeRange: batchTimeRange }))
}


module.exports = {
  arrayToObject,
  batchQuery
}