const _                                      = require('lodash')
const TABLE_CONFIG                           = require('./table-config.json')
const { model }                              = require('../utils/db')
const { QueryTypes }                         = require('sequelize')
const { parseNumericArray }                  = require('../helpers/numbers')
const { arrayToObject }                      = require('./helpers')
const { getParameterInfo, getParameterList } = require('../configuration/parameter-information/parameters')
const { optimizeQueryList }                  = require('./query-optimizer')
const { getEquipmentList }                   = require('../configuration')


const isCsvField = (device, field) => {
  const parameterInfo = getParameterInfo(device, field);
  if (!parameterInfo || parameterInfo.dataType != 'csv') return false

  return true; 
}


const getCsvFieldList = (device) => {
  const fieldList = getParameterList(device)
  return fieldList.filter(field => {
    parameterInfo = getParameterInfo(device, field)
    if (!parameterInfo || parameterInfo.dataType != 'csv') return false

    return true;
  })
}


const parseEquipmentId = (device, equipmentId) => {
  const { dataTableUniqueFields } = TABLE_CONFIG[device]  

  return _.zipObject(dataTableUniqueFields, equipmentId.split('-'))
}


const getDataTableName = (device) => {
  return TABLE_CONFIG[device].dataTableName
}


const getDataTableUniqueFields = (device) => {
  return TABLE_CONFIG[device].dataTableUniqueFields
}


const isNullableField = (device) => {
  return device.trim().startsWith('$')
}


const constructEquipmentClause = (device, equipmentIds) => {
  const ORList = equipmentIds
    .map(equipmentId => parseEquipmentId(device, equipmentId))
    .map(parsedId => {
      const ANDList = Object.entries(parsedId).map(([fieldName, value]) => `${fieldName} = ${value}`)
      return `( ${ANDList.join(' AND ')} )`
    })

  return ORList && `AND ( ${ORList.join(' OR ')} )`;
}


const makeQuery = ({ device, plantId, fields, equipmentIds, timeRange: [startUnixTime, endUnixTime] }) => {
  if (!startUnixTime || !endUnixTime) throw new Error('timeRange is required!')
  if (!fields)                        throw new Error('fields is required')

  const dataTableName         = getDataTableName(device)
  const dataTableUniqueFields = getDataTableUniqueFields(device)    
  const equipmentClause       = equipmentIds && constructEquipmentClause(device, equipmentIds) || ''
  const fieldList             = fields.map(field => {
    if (isCsvField(device, field)) return field;
    if (isNullableField(field)) return `Null AS ${field.replace('$', '')} `;

    return `Round(${field}, 2) AS ${field}`
  })

  const sQuery = `
    SELECT 
      plant_id,
      '${device}'                             AS device,
      sensor_time                             AS timestamp, 
      CONCAT_WS('-',${dataTableUniqueFields}) AS device_id,
      ${fieldList}
    FROM
      ${dataTableName}
    WHERE
      plant_id IN (${plantId}) 
      ${equipmentClause}
      AND sensor_time BETWEEN ${startUnixTime} AND ${endUnixTime - 1}
  `
  return sQuery
}


const unionAndQueryData = (optimizedQueryList) => {
  const uniqueDevices = _.uniq(_.map(optimizedQueryList, 'device'))

  if (uniqueDevices.length > 1) throw new Error('Cannot perform union for different devices')

  let uniqueFieldList = []

  optimizedQueryList.forEach(({fields}) => {
    uniqueFieldList = _.union(uniqueFieldList, fields)
  })

  optimizedQueryList.forEach((query) => {
    const diffSet = new Set(_.difference(uniqueFieldList, query.fields))
    const unionFields = uniqueFieldList.map(field => {
      return diffSet.has(field) ? `$${field}`: field
    })

    query.fields = unionFields
  })

  const unionQuery = optimizedQueryList.map(query => makeQuery(query)).join(' \n UNION \n ')
  return model.query(unionQuery, {
    type: QueryTypes.SELECT
  })
}


const verifyEquipmentIds = (query) => {
  const { plantId, equipmentIds, device } = query
  if(equipmentIds && equipmentIds.length) return query;

  const equipmentList = getEquipmentList({ plantId, device })

  if (!equipmentList) return null

  query.equipmentIds = equipmentList
  return query
}


const getSensorData = async (optimizedQueryList, { expandCsv = true } = {}) => {
  const { device } = optimizedQueryList[0]
  const result = await unionAndQueryData(optimizedQueryList)
  const csvFieldList = getCsvFieldList(device)

  if (!csvFieldList || !csvFieldList.length) return result

  result.forEach(row => {
    csvFieldList.forEach((field) => {
      const csv = row[field]
      if (!csv) return;

      const numericArray = parseNumericArray(csv)

      if (expandCsv) {
        const parameterObj = arrayToObject(numericArray, field)
        delete row[field]
        Object.assign(row, parameterObj)
        return;
      }

      row[field] = numericArray
    })
  })

  return result
}


async function* generateSensorData(queryParameterList, { batchSize = 1000 } = {}) {
  queryParameterList = queryParameterList.map(query => verifyEquipmentIds(query)).filter(query => query)
  const optimzedQueryList = optimizeQueryList(queryParameterList, batchSize)

  for (const optimizedQuery of optimzedQueryList) {
    const data = await getSensorData(optimizedQuery)
    yield data;
  }
}


module.exports = {
  generateSensorData
}

