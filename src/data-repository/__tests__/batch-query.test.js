const { batchQuery } = require("../helpers");

test('Batch query according to the batchInterval (seconds)', () => {
  const startUnixTime = 1632468600;  // September 24, 2021 7:30:00 AM GMT
  const endUnixTime = 1632763800 // September 27, 2021 5:30:00 PM GMT

  const queryParameter = {
      plantId: 103,
      device: 'inverter',
      timeRange: [startUnixTime, endUnixTime]
  }
  const batchInterval = 86400 // 24 hours
  const expectedOutput = [
    {
      plantId: 103,
      device: 'inverter',
      timeRange: [1632468600, 1632555000]
    },
    {
      plantId: 103,
      device: 'inverter',
      timeRange: [1632555001, 1632641401]
    },
    {
      plantId: 103,
      device: 'inverter',
      timeRange: [1632641402, 1632727802] 
    },
    {
      plantId: 103,
      device: 'inverter',
      timeRange: [1632727803, 1632763800] 
    }
  ]
  
  const result = batchQuery(queryParameter, batchInterval)
  expect(result).toEqual(expectedOutput)
})


test('Throws error if startUnixTime is more than endUnixTime', () => {
  const startUnixTime = 1632763800;  // September 24, 2021 7:30:00 AM GMT
  const endUnixTime = 1632468600// September 27, 2021 5:30:00 PM GMT

  const queryParameter = {
      plantId: 103,
      device: 'inverter',
      timeRange: [startUnixTime, endUnixTime]
  }
  const batchInterval = 86400 // 24 hours

  expect(() => {
    batchQuery(queryParameter, batchInterval)
  }).toThrow()
})