const { optimizeQueryList } = require("../query-optimizer")

test('it shoud club all for same device if expected row count of all falls under rowlimit', () => {
  const testData = [
    {
      plantId: 103,
      device: 'inverter',
      equipmentIds: ['1'],
      timeRange: [0, 600]
    },
    {
      plantId: 104,
      device: 'inverter',
      equipmentIds: ['1'],
      timeRange: [1000, 2000]
    }
  ]

  const optimizedQueryList = optimizeQueryList(testData, 30)

  const expectedOutput = [
    [
      {
        plantId: 103,
        device: 'inverter',
        equipmentIds: ['1'],
        timeRange: [0, 600]
      },
      {
        plantId: 104,
        device: 'inverter',
        equipmentIds: ['1'],
        timeRange: [900, 2100]
      }
    ]
  ]

  expect(optimizedQueryList).toEqual(expectedOutput)
})

test('it should optimize by batching by expected row count based on row limit', () => {
  const testData = [
    {
      plantId: 103,
      device: 'inverter',
      equipmentIds: ['1'],
      timeRange: [0, 600]
    },
    {
      plantId: 104,
      device: 'inverter',
      equipmentIds: ['1'],
      timeRange: [1000, 2000]
    }
  ]

  const optimizedQueryList = optimizeQueryList(testData, 20)

  const expectedOutput = [
    [
      {
        plantId: 103,
        device: 'inverter',
        equipmentIds: ['1'],
        timeRange: [0, 600]
      },
      {
        plantId: 104,
        device: 'inverter',
        equipmentIds: ['1'],
        timeRange: [900, 1500]
      }
    ],[
      {
        plantId: 104,
        device: 'inverter',
        equipmentIds: ['1'],
        timeRange: [1500, 2100]
      }
    ],
  ]

  expect(optimizedQueryList).toEqual(expectedOutput)
})

test('works for multiple equipmentIds', () => {
  const testData = [
    {
      plantId: 103,
      device: 'inverter',
      equipmentIds: ['1', '2', '3'],
      timeRange: [0, 600]
    },
    {
      plantId: 104,
      device: 'inverter',
      equipmentIds: ['1', '2'],
      timeRange: [1000, 2000]
    }
  ]

  const optimizedQueryList = optimizeQueryList(testData, 25)
  const expectedOutput = [
    [
      {
        plantId: 103,
        device: 'inverter',
        equipmentIds: ['1', '2'],
        timeRange: [0, 600]
      },
      {
        plantId: 103,
        device: 'inverter',
        equipmentIds: ['3'],
        timeRange: [0, 300]
      },
    ],[
      {
        plantId: 103,
        device: 'inverter',
        equipmentIds: ['3'],
        timeRange: [300, 600]
      },
      {
        plantId: 104,
        device: 'inverter',
        equipmentIds: ['1'],
        timeRange: [900, 2100]
      },
    ],[
      {
        plantId: 104,
        device: 'inverter',
        equipmentIds: ['2'],
        timeRange: [900, 2100]
      }
    ],
  ]

  expect(optimizedQueryList).toEqual(expectedOutput)
})


test('works for multiple equipmentIds and devices', () => {
  const testData = [
    {
      plantId: 103,
      device: 'inverter',
      equipmentIds: ['1', '2', '3'],
      timeRange: [0, 600]
    },
    {
      plantId: 104,
      device: 'inverter',
      equipmentIds: ['1', '2'],
      timeRange: [1000, 2000]
    },
    {
      plantId: 103,
      device: 'mfm',
      equipmentIds: ['10', '11'],
      timeRange: [1000, 1500]
    },
    {
      plantId: 104,
      device: 'mfm',
      equipmentIds: ['10', '11'],
      timeRange: [100, 200]
    },
  ]

  const optimizedQueryList = optimizeQueryList(testData, 25)
  const expectedOutput = [
    [
      {
        plantId: 103,
        device: 'inverter',
        equipmentIds: ['1', '2'],
        timeRange: [0, 600]
      },
      {
        plantId: 103,
        device: 'inverter',
        equipmentIds: ['3'],
        timeRange: [0, 300]
      },
    ],[
      {
        plantId: 103,
        device: 'inverter',
        equipmentIds: ['3'],
        timeRange: [300, 600]
      },
      {
        plantId: 104,
        device: 'inverter',
        equipmentIds: ['1'],
        timeRange: [900, 2100]
      },
    ],[
      {
        plantId: 104,
        device: 'inverter',
        equipmentIds: ['2'],
        timeRange: [900, 2100]
      }
    ],[
      {
        plantId: 103,
        device: 'mfm',
        equipmentIds: ['10', '11'],
        timeRange: [900, 1500]
      },
      {
        plantId: 104,
        device: 'mfm',
        equipmentIds: ['10'],
        timeRange: [0, 300]
      }
    ],
    [
      {
        plantId: 104,
        device: 'mfm',
        equipmentIds: ['11'],
        timeRange: [0, 300]
      }
    ]
  ]

  expect(optimizedQueryList).toEqual(expectedOutput)
})

