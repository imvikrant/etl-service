const { chunk } = require('lodash')
const { insertAggregateData } = require('./data-repository/insert-aggregate-data')
const { isNumber } = require('./helpers/numbers')
const PromisePool = require('@supercharge/promise-pool')

let bufferedData = []


const chunkAndInsert = async (data, chunkSize, parallelLimit = 1) => {
  const chunkedData = chunk(data, chunkSize)
  await PromisePool
    .withConcurrency(parallelLimit)
    .for(chunkedData)
    .process(async (cData, index) => {
      await insertAggregateData(cData)
    })
}

const loadData = async (sensorData, { bufferSize, concurrency = 1 } = {}) => {
  const chunkSize = 1000;
  if (!sensorData || !sensorData.data || !sensorData.data.length) {
    const rowsCount = bufferedData.length;
    if (bufferedData.length) {
      await chunkAndInsert(bufferedData, chunkSize, concurrency)
    }
    bufferedData = []
    return rowsCount;
  }

  const { plantId, device, deviceId, data } = sensorData

  const dataToInsert = data.flatMap(({ timestamp, ...parameters }) => {
    return Object.entries(parameters).flatMap(([parameter, value]) => {
      return ({ timestamp, plantId, device, deviceId, parameter, value })
    })
  })

  bufferedData.push(...dataToInsert)

  if (!bufferSize || !isNumber(bufferSize, bufferSize)) {
    const rowsCount = bufferedData.length
    await chunkAndInsert(bufferedData, chunkSize, concurrency)
    bufferedData = []
    return rowsCount;
  }

  if (bufferedData.length >= bufferSize) {
    const partialData = bufferedData.splice(0, Math.floor(bufferedData.length / bufferSize) * bufferSize)
    const rowsCount = partialData.length
    await chunkAndInsert(partialData, chunkSize, concurrency)
    return rowsCount
  }

  return 0
}


module.exports = {
  loadData
}