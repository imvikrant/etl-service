const { aggregateDelayedData } = require('.');
const { getPlantList } = require('../src/configuration/plants');
const { getUnixtime, startOfDay, subtractDays, now } = require('../src/helpers/date');

module.exports.run = async (event) => {
  const timezone      = event.timezone 
  const plants        = await getPlantList({ timezone })
  const currentTime   = now(timezone) 
  const startUnixTime = getUnixtime(startOfDay(currentTime)) 
  const endUnixTime   = getUnixtime(currentTime)

  try {
    await aggregateDelayedData({ 
      plants, 
      timeRange: [startUnixTime, endUnixTime],
      sensorTimeRange: [getUnixtime(startOfDay(subtractDays(currentTime, 7))), endUnixTime] 
    })
    return { message: 'Function executed successfully!!', event };
  } catch (e) {
    console.error(e)
    return { message: 'Function execution failed!', event, error: e };
  }
};