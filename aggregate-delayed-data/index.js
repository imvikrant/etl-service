const { QueryTypes } = require('sequelize')
const { model } = require('../src/utils/db');
const { etl } = require('../index');
const { Datetime, getUnixtime, startOf, now } = require('../src/helpers/date');
const { updatePlantData } = require('../plant-level-continuous-aggregate/update-plant-data');
const { aggregateTopLevelData } = require('../top-level-aggregate');
const { computeDataForDerivedParameters } = require('../plant-level-computed-fields');


const fetchMissedAggregationPackets = async ({
  plants,
  timeRange,
  sensorTimeRange,
}) => {

  const [startUnixTime, endUnixTime] = timeRange
  const [sensorStartUnixTime, sensorEndUnixTime] = sensorTimeRange || []

  const sQuery = `
    SELECT 
      plant_id,
      device,
      GROUP_CONCAT(device_id) device_ids,
      sum(count) total,
      start_unixtime,
      end_unixtime
    FROM (
        SELECT plant_id,
          device,
          device_id,
          COUNT(*) count,
          MIN(sensor_time) start_unixtime,
          MAX(sensor_time) end_unixtime
        FROM tbl_utc_late_packet_data
        WHERE added_time BETWEEN ${startUnixTime} AND ${endUnixTime}
        ${sensorTimeRange ? `AND sensor_time BETWEEN ${sensorStartUnixTime} AND ${sensorEndUnixTime}` : ''}
          AND plant_id IN (${plants.join(', ')})
          AND added_time > CEILING(sensor_time / 300) * 300 + 60
        GROUP BY plant_id,
          device,
          device_id
      ) SUB_Q
    GROUP BY plant_id,
      device
  `

  return model.query(sQuery, {
    type: QueryTypes.SELECT
  })
}

const reaggregateLatePacketData = async (queryParameterList, timezone) => {
  const etlOptions = {
    writeConcurrency: 5,
    writeBatchSize  : 10000,
    readBatchSize   : 20000,
  }
  const validPrDevicesSet = new Set(['inverter', 'mfm', 'energy meter'])
  const dt = now(timezone)
  const startOfDayUnix = getUnixtime(startOf(dt, 'day'))
  const endOfPreviousDayUnix = startOfDayUnix - 1;

  const insolationQueryList = queryParameterList
    .filter(query => query.device == 'weather station')
    .map(({ plantId, device, timeRange }) => ({ plantId, device, timeRange }))

  const prQueryList = queryParameterList
    .filter(query => validPrDevicesSet.has(query.device))
    .map(({ plantId, device, timeRange }) => ({ plantId, device, timeRange }))

  // Filter out days for top level aggregate if it falls in the current day. since the aggregate for current day
  // will run eventually after late packet handling
  const topLevelQueryList = queryParameterList
    .filter(({ timeRange }) => timeRange[0] < startOfDayUnix)
    .map((timeRange, ...query) => ({ timeRange: [timeRange[0], Math.min(endOfPreviousDayUnix, timeRange[1])], ...query }))

  // Re-aggregate 5 minute device and plant data
  await etl(queryParameterList, etlOptions)
  await updatePlantData(queryParameterList)  

  // Update derived parameters (pr, insolation)
  insolationQueryList.length && await computeDataForDerivedParameters(insolationQueryList)
  prQueryList.length && await computeDataForDerivedParameters(prQueryList)

  // Update daily, monthly, yearly data
  topLevelQueryList.length && await aggregateTopLevelData(queryParameterList)

}

const aggregateDelayedData = async ({
  plants,
  timeRange,
  sensorTimeRange,
  timezone = 'Asia/Kolkata'
} = {}) => {

  const [, endUnixTime] = timeRange
  let result = await fetchMissedAggregationPackets({ plants, timeRange, sensorTimeRange })

  const queryParameterList = result.map(({
    plant_id: plantId,
    device,
    device_ids: deviceIds,
    start_unixtime: startOfLatePacketUnixtime,
  }) => {
    // Aggregate from the start of the day corresponding to the time of the first late packet
    const dt = Datetime(startOfLatePacketUnixtime, timezone)
    const startUnixTime = getUnixtime(startOf(dt, 'day'))

    return ({
      timeRange: [startUnixTime, endUnixTime],
      plantId,
      device,
      equipmentIds: deviceIds.split(','),
    })
  })

  if (!queryParameterList.length) return;

  await reaggregateLatePacketData(queryParameterList, timezone)

}

module.exports = {
  aggregateDelayedData
}
