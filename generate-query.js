const { getPlantList, getPlantWiseDeviceList } = require('./src/configuration/plants')

const formUltimateQueryParameterList = async ({ plantList, deviceList, startUnixTimestamp, endUnixTimestamp, window = 300 } = {}) => {
  if (!startUnixTimestamp) throw new Error('startUnixTimestamp is required!')
  if (!endUnixTimestamp) throw new Error('endUnixTimestamp is required!')

  const startTime = Math.floor(startUnixTimestamp / window) * window 
  const endTime   = Math.ceil(endUnixTimestamp / window) * window 

  const plants = plantList || await getPlantList()
  const plantWiseDevices = await getPlantWiseDeviceList(plants)

  const queryParameterList = []

  Object.entries(plantWiseDevices).forEach(([plantId, devices]) => {
    deviceList && (devices = deviceList);

    devices.forEach(device => {
      queryParameterList.push({
        timeRange: [startTime, endTime],
        plantId,
        device,
      })
    })
  })
  return queryParameterList
}


module.exports = {
  formUltimateQueryParameterList
}