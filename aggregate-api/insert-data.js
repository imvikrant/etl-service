const { QueryTypes } = require('sequelize')
const { model } = require('../src/utils/db')
const _ = require('lodash')

const tableMapping = {
  'minutely': 'tbl_utc_device_aggregate_data',
  'daily'   : 'tbl_utc_daily_device_aggregate_data',
  'monthly' : 'tbl_utc_monthly_device_aggregate_data',
  'yearly'  : 'tbl_utc_yearly_device_aggregate_data'
}

const plantTableMapping = {
  'minutely': 'tbl_utc_plant_aggregate_data',
  'daily'   : 'tbl_utc_daily_plant_aggregate_data',
  'monthly' : 'tbl_utc_monthly_plant_aggregate_data',
  'yearly'  : 'tbl_utc_yearly_plant_aggregate_data'
}


const parseNumber = (value) => {
  const num = parseFloat(value)
  return isNaN(num) ? null : num
}


const insertAggregateData = async (data, dataInterval, chunkSize = 1000) => {
  if (!data || !data.length) return;

  const chunkedData = _.chunk(data, chunkSize)

  for (let d of chunkedData) {
    const values = d.map(
      ({ timestamp, plantId, device, deviceId, parameter, value }) => 
        `(${timestamp}, ${plantId}, '${device}', '${deviceId}', '${parameter}', ${parseNumber(value)})`
    )

    const sQuery = `
      INSERT INTO 
        ${tableMapping[dataInterval]}(sensor_time, plant_id, device, device_id, parameter, value)
      VALUES 
        ${values.join(`,\n`)}
      ON DUPLICATE KEY 
        UPDATE
          value = VALUES(value)
    `
    await model.query(sQuery, {
      type: QueryTypes.INSERT
    })
  }
}


const insertPlantAggregateData = async (data, dataInterval, chunkSize = 1000) => {
  if (!data || !data.length) return;

  const chunkedData = _.chunk(data, chunkSize)

  for (let d of chunkedData) {
    const values = d.map(
      ({ timestamp, plantId, device, parameter, value }) => 
        `(${timestamp}, ${plantId}, '${device}', '${parameter}', ${parseNumber(value)})`
    )

    const sQuery = `
      INSERT INTO 
        ${plantTableMapping[dataInterval]}(sensor_time, plant_id, device, parameter, value)
      VALUES 
        ${values.join(`,\n`)}
      ON DUPLICATE KEY 
        UPDATE
          value = VALUES(value)
    `
    await model.query(sQuery, {
      type: QueryTypes.INSERT
    })
  }
}


module.exports = {
  insertAggregateData,
  insertPlantAggregateData
}