const { Datetime, isBefore, getUnixtime, endOf, startOf, diff, clone, min, max, add } = require("../../src/helpers/date");

const acceptedUnits = ['day', 'month', 'year'];

const splitTimeRange = (timeRange, unit, timezone = 'Asia/Kolkata') => {

  if (!acceptedUnits.includes(unit)) throw new Error('Invalid unit')

  const [start, end] = timeRange;

  const startTime = Datetime(start, timezone)
  const endTime = Datetime(end, timezone)
  const timeDiff = diff(endTime, startTime, unit)

  if (timeDiff < 0) throw new Error('Invalid Range')

  if (timeDiff == 0) return [timeRange]

  const range = []

  let timeTraveller = clone(startTime);

  while (isBefore(timeTraveller, endTime)) {
    const startOfDayTime = startOf(timeTraveller, unit)
    const endOfDayTime = endOf(timeTraveller, unit)
    range.push([
      getUnixtime(max(startOfDayTime, startTime)),
      getUnixtime(min(endOfDayTime, endTime))
    ])
    timeTraveller = startOf(add(timeTraveller, 1, unit), unit)
  }

  return range;
}


const splitQuery = (query, unit) => {
  const { timeRange, timezone } = query;

  const splitTimeArray = splitTimeRange(timeRange, unit, timezone)

  return splitTimeArray.map(timeRange => ({
    ...query,
    timeRange
  }))

}

module.exports = {
  splitQuery
}
