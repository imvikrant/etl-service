const { getParameterInfo } = require("../../src/configuration/parameter-information/parameters");

const getAggregateFieldMapping = (device, fields, overrideMapping) => {
  const aggregateFieldsMap = {}

  fields.forEach(field => {
    const paramInfo = getParameterInfo(device, field);
    const paramAggMethod = paramInfo && paramInfo.aggregateMethod;
    const aggMethodOveride = overrideMapping && overrideMapping[device] && overrideMapping[device][field]

    const aggMethod = aggMethodOveride || paramAggMethod;

    if (!aggMethod) return;

    aggregateFieldsMap[field] = aggMethod == 'increase' ? 'sum' : aggMethod
  })

  return aggregateFieldsMap
}

module.exports = {
  getAggregateFieldMapping
} 