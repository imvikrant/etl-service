const { QueryTypes } = require("sequelize")
const { model } = require("../src/utils/db")
const queryDevice = require('./queries/device-level-queries')
const queryPlant = require('./queries/plant-level-queries')


const queryApi = async ({ device, plantId, equipmentIds, timeRange, fields }, dataInterval, level) => {
  let sQuery = null; 
  if (level === 'device') {
    sQuery = queryDevice.queryData({dataInterval, device, plantId, equipmentIds, timeRange, fields})
  }
  if (level === 'plant') {
    sQuery = queryPlant.queryData({dataInterval, device, plantId, timeRange, fields})
  }

  return model.query(sQuery, {
    type: QueryTypes.SELECT
  })
}


const aggregateApi = ({ device, plantId, equipmentIds, timeRange, aggregateFieldsMap, groupBy }, dataInterval, level) => {
  const fieldsMapping = {
    latest: [],
    average: [],
    sum: []
  }

  Object.entries(aggregateFieldsMap).forEach(([field, aggregateMethod]) => {
    if (!fieldsMapping[aggregateMethod]) return;

    fieldsMapping[aggregateMethod].push(field)
  })

  const queryUnionArray = []

  if (level === 'device') {
    if (fieldsMapping.latest.length) {
      queryUnionArray.push(queryDevice.queryLatest({dataInterval, device, plantId, equipmentIds, timeRange, fields: fieldsMapping.latest , groupBy}))
    }
    if (fieldsMapping.sum.length) {
      queryUnionArray.push(queryDevice.querySum({ dataInterval,device, plantId, equipmentIds, timeRange, fields: fieldsMapping.sum, groupBy  }))
    }
    if (fieldsMapping.average.length) {
      queryUnionArray.push(queryDevice.queryAvg({ dataInterval,device, plantId, equipmentIds, timeRange, fields: fieldsMapping.average, groupBy }))
    }
  }

  if (level === 'plant') {
    if (fieldsMapping.latest.length) {
      queryUnionArray.push(queryPlant.queryLatest({dataInterval, device, plantId, timeRange, fields: fieldsMapping.latest , groupBy}))
    }
    if (fieldsMapping.sum.length) {
      queryUnionArray.push(queryPlant.querySum({ dataInterval,device, plantId, timeRange, fields: fieldsMapping.sum, groupBy  }))
    }
    if (fieldsMapping.average.length) {
      queryUnionArray.push(queryPlant.queryAvg({ dataInterval,device, plantId, timeRange, fields: fieldsMapping.average, groupBy }))
    }
  }

  const sQuery = queryUnionArray.join('\n UNION \n');

  return model.query(sQuery, {
    type: QueryTypes.SELECT
  })
}


async function* generateMinutelyPlantAggregateData(aggregateQueryList, dataInterval) {
  aggregateQueryList = aggregateQueryList.map(query => ({...query, groupBy: ['parameter', 'sensor_time'] }))
  for (let query of aggregateQueryList) {
    const data = await aggregateApi(query, 'minutely', 'device')
    yield data;
  }
}

module.exports = {
  aggregateApi,
  queryApi,
  generateMinutelyPlantAggregateData
}