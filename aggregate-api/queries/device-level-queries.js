const tableMapping = {
  'minutely': 'tbl_utc_device_aggregate_data',
  'daily'   : 'tbl_utc_daily_device_aggregate_data',
  'monthly' : 'tbl_utc_monthly_device_aggregate_data',
  'yearly'  : 'tbl_utc_yearly_device_aggregate_data'
}
const queryData = ({ dataInterval = 'minutely', device, plantId, equipmentIds, fields, timeRange } = {}) => {
  const [fromUnixTime, toUnixTime] = timeRange

  if (!tableMapping[dataInterval]) return;

  const sQuery = `
    SELECT
      sensor_time timestamp,
      plant_id,
      device,
      device_id,
      parameter,
      ROUND(value, 3) value 
    FROM
      ${tableMapping[dataInterval]}
    WHERE
      sensor_time BETWEEN '${fromUnixTime}' AND '${toUnixTime}'
      AND device = '${device}'
      AND parameter IN (${fields.map(field => `'${field}'`).join()})
      AND device_id IN (${equipmentIds.map(id => `'${id}'`).join()})
      AND plant_id = ${plantId}
  ` 

  return sQuery
}

const queryAvg = ({ dataInterval = 'minutely', device, plantId, equipmentIds, fields, timeRange, groupBy = ['device_id', 'parameter']} = {}) => {
  const [fromUnixTime, toUnixTime] = timeRange

  if (!tableMapping[dataInterval]) return;

  const sQuery = `
    SELECT
      sensor_time timestamp,
      plant_id,
      device,
      device_id,
      parameter,
      ROUND(AVG(value), 3) value 
    FROM
      ${tableMapping[dataInterval]}
    WHERE
      sensor_time BETWEEN '${fromUnixTime}' AND '${toUnixTime}'
      AND device = '${device}'
      AND parameter IN (${fields.map(field => `'${field}'`).join()})
      AND device_id IN (${equipmentIds.map(id => `'${id}'`).join()})
      AND plant_id = ${plantId}
    GROUP BY
      ${groupBy.join(', ')}
  ` 

  return sQuery
}

const querySum = ({ dataInterval = 'minutely', device, plantId, equipmentIds, fields, timeRange, groupBy = ['device_id', 'parameter']} = {}) => {
  const [fromUnixTime, toUnixTime] = timeRange

  if (!tableMapping[dataInterval]) return;

  const sQuery = `
    SELECT
      sensor_time timestamp,
      plant_id,
      device,
      device_id,
      parameter,
      ROUND(SUM(value), 3) value 
    FROM
      ${tableMapping[dataInterval]}
    WHERE
      sensor_time BETWEEN '${fromUnixTime}' AND '${toUnixTime}'
      AND device = '${device}'
      AND parameter IN (${fields.map(field => `'${field}'`).join()})
      AND device_id IN (${equipmentIds.map(id => `'${id}'`).join()})
      AND plant_id = ${plantId}
    GROUP BY
      ${groupBy.join(', ')}
  ` 

  return sQuery
}


const queryLatest = ({ dataInterval = 'minutely', device, plantId, equipmentIds, fields, timeRange, groupBy = ['device_id', 'parameter']} = {}) => {
  const [fromUnixTime, toUnixTime] = timeRange

  if (!tableMapping[dataInterval]) return;

  const sQuery = `
    SELECT 
      A.sensor_time timestamp,
      A.plant_id,
      A.device,
      A.device_id,
      A.parameter,
      ROUND(A.value, 3) value
    FROM
      ${tableMapping[dataInterval]} A
        JOIN
      (SELECT 
        plant_id, device, device_id, parameter, MAX(sensor_time) sensor_time
      FROM
        ${tableMapping[dataInterval]}
      WHERE
        sensor_time BETWEEN '${fromUnixTime}' AND '${toUnixTime}'
        AND device = '${device}'
        AND parameter IN (${fields.map(field => `'${field}'`).join()})
        AND device_id IN (${equipmentIds.map(id => `'${id}'`).join()})
        AND plant_id = ${plantId}
        AND value IS NOT NULL
      GROUP BY 
        ${groupBy.join(', ')}) B 
      ON 
      ( A.plant_id = B.plant_id
        AND A.device = B.device
        AND A.device_id = B.device_id
        AND A.parameter = B.parameter
        AND A.sensor_time = B.sensor_time )
  ` 

  return sQuery;
}


module.exports = {
  queryData,
  queryAvg,
  queryLatest,
  querySum
}