const { generatePlantInsolationData } = require("./insolation")
const { insertPlantAggregateData, insertAggregateData } = require('../aggregate-api/insert-data')
const { generatePlantPrData, generateEquipmentPrData } = require("./pr")
const { loadConfiguration } = require("../src/configuration");

const validPrDevicesSet = new Set(['inverter', 'mfm', 'energy meter'])

const computeInsolation = async (queryList) => {
  const plantInsolationDataGenerator = generatePlantInsolationData(queryList)

  for await (const plantInsolationData of plantInsolationDataGenerator) {
    if (!plantInsolationData || !plantInsolationData.length) continue

    await insertPlantAggregateData(plantInsolationData, 'minutely')
  }
}


const computePlantPr = async (queryList) => {
  const plantPrDataGenerator = generatePlantPrData(queryList)

  for await (const plantPrData of plantPrDataGenerator) {
    if (!plantPrData || !plantPrData.length) continue

    await insertPlantAggregateData(plantPrData, 'minutely')
  }

}


const computeEquipmentPr = async (queryList) => {
  const equipmentPrDataGenerator = generateEquipmentPrData(queryList)

  for await (const equipmentPrData of equipmentPrDataGenerator) {
    if (!equipmentPrData || !equipmentPrData.length) continue

    await insertAggregateData(equipmentPrData, 'minutely')
  }
}

const computeDataForDerivedParameters = async (queryList) => {
  await loadConfiguration({ plants: queryList.map(({ plantId }) => plantId) })
  const insolationQueryList = []
  const prQueryList = []

  queryList.forEach(({ plantId, timeRange, device }) => {
    if (device === 'weather station') {
      return insolationQueryList.push({ plantId, timeRange, device })
    }

    if (validPrDevicesSet.has(device)) {
      return prQueryList.push({ plantId, device, timeRange })
    }
  })

  try {
    insolationQueryList.length && await computeInsolation(insolationQueryList)
    prQueryList.length && await computeEquipmentPr(prQueryList)
    prQueryList.length && await computePlantPr(prQueryList)
    console.log('Derived Plant Data computed')
  } catch (e) {
    console.log('ERROR computing derived fields', e)
  }
}


module.exports = {
  computeDataForDerivedParameters
}