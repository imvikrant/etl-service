const { splitQuery } = require("../aggregate-api/utils/split-query")
const { getWsConfig, loadConfiguration, getPlantInfo, getEquipmentList, getEquipment } = require("../src/configuration")
const { getUnixtime, startOfDay, Datetime } = require('../src/helpers/date')
const { getCumulativeDailySum, mergeCollectionByKey } = require("./utils")

const validDeviceSet = new Set(['inverter', 'energy meter', 'mfm'])

const getInsolationSourceTypeForPR = (plantId) => {
  const { prType } = getWsConfig({ plantId }) || {}

  if (prType === 'POA') return 'poa_insolation';
  if (prType === 'GHI') return 'ghi_insolation';
  if (prType === 'RAD') return 'rad_insolation';
}


const getGenerationParameterName = (device) => {
  if (device === 'inverter') return 'energy';
  if (device === 'energy meter' || device === 'mfm') return 'active_total_export_cumulative'
}


const sanitizeQueryList = (queryList) => {
  const splitQueries = queryList.flatMap(query => splitQuery(query, 'day'))

  return splitQueries
}


const getEquipmentPr = async ({ plantId, device, equipmentIds, timeRange }) => {
  if (!validDeviceSet.has(device)) throw new Error('PR not available for the device')

  let insolationType = getInsolationSourceTypeForPR(plantId)
  let generationField = getGenerationParameterName(device)

  if (!generationField || !insolationType) return null;

  if (!equipmentIds) {
    equipmentIds = getEquipmentList({ plantId, device })
  }

  const energyResult = await getCumulativeDailySum({ device, plantId, timeRange, equipmentIds, fields: [generationField] }, 'minutely', 'device')
  const insolationResult = await getCumulativeDailySum({ device: 'weather station', plantId, fields: [insolationType], timeRange }, 'minutely', 'plant')

  if (!insolationResult || !insolationResult.length) return;
  if (!energyResult || !energyResult.length) return;

  const insolationData = insolationResult && insolationResult[0].map(({ timestamp, value }) => ({ timestamp, insolation: value }))

  return energyResult.flatMap(data => {
    if (!data || !data.length) return

    const { device_id: deviceId } = data[0];
    const { dcCapacity } = device === 'inverter' ? getEquipment({ plantId, device, deviceId }) : getPlantInfo({ plantId })

    const energyData = data.map(({ device_id, timestamp, value }) => ({ device_id, timestamp, energy: value }))
    const mergedData = mergeCollectionByKey(energyData, insolationData, 'timestamp')

    return mergedData.map(({ timestamp, energy, insolation }) => {
      const pr = energy && insolation && dcCapacity ? (energy / (insolation * dcCapacity) * 100) : null;

      return { timestamp, plantId, device, deviceId, parameter: 'pr', value: pr }
    })
  })
}


const getPlantPr = async ({ plantId, device, timeRange }) => {
  if (!validDeviceSet.has(device)) throw new Error('PR not available for the device')

  let insolationType = getInsolationSourceTypeForPR(plantId)
  let generationField = getGenerationParameterName(device)
  const { dcCapacity } = getPlantInfo({ plantId }) || {}

  if (!generationField || !insolationType || !dcCapacity) return null;

  const energyResult = await getCumulativeDailySum({ device, plantId, timeRange, fields: [generationField] }, 'minutely', 'plant')
  const insolationResult = await getCumulativeDailySum({ device: 'weather station', plantId, fields: [insolationType], timeRange }, 'minutely', 'plant')
  
  if (!insolationResult || !insolationResult.length) return;
  if (!energyResult || !energyResult.length) return;

  const energyData = energyResult[0].map(({ timestamp, value }) => ({ timestamp, energy: value }))
  const insolationData = insolationResult[0].map(({ timestamp, value }) => ({ timestamp, insolation: value }))

  const mergedData = mergeCollectionByKey(energyData, insolationData, 'timestamp')


  return mergedData.map(({ timestamp, energy, insolation }) => {
    const pr = energy && insolation && dcCapacity ? (energy / (insolation * dcCapacity) * 100) : null;

    return { timestamp, plantId, device, parameter: 'pr', value: pr }
  })
}


async function* generatePlantPrData(queryList) {
  const plants = queryList.map(query => query.plantId);
  await loadConfiguration({ plants })
  const sanitizedQueryList = sanitizeQueryList(queryList)

  for (let query of sanitizedQueryList) {
    const data = await getPlantPr(query)
    yield data;
  }
}


async function* generateEquipmentPrData(queryList) {
  const plants = queryList.map(query => query.plantId);
  await loadConfiguration({ plants })
  const sanitizedQueryList = sanitizeQueryList(queryList)

  for (let query of sanitizedQueryList) {
    try {
      const data = await getEquipmentPr(query)
      yield data;
    } catch(e) {
      console.log(`Error computing Equipment Pr for plant ${query && query.plantId}`,  e)
      yield null;
    }
  }
}


module.exports = {
  generatePlantPrData,
  generateEquipmentPrData
}