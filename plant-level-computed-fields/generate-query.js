const { getPlantWiseDeviceList, getPlantList } = require("../src/configuration/plants")

const generateInsolationQueryList = async ({ plantList, startUnixTimestamp, endUnixTimestamp, window = 300 }) => {
  if (!startUnixTimestamp) throw new Error('startUnixTimestamp is required!')
  if (!endUnixTimestamp) throw new Error('endUnixTimestamp is required!')

  const startTime = Math.floor(startUnixTimestamp / window) * window 
  const endTime   = Math.ceil(endUnixTimestamp / window) * window
  const plants = plantList || await getPlantList()
  const plantWiseDevices = await getPlantWiseDeviceList(plants)

  const plantsWithWs = plants.filter(plantId => !!(plantWiseDevices[plantId] && plantWiseDevices[plantId].includes('weather station')))

  return plantsWithWs.map(plantId => ({ plantId, timeRange: [startTime, endTime], device: 'weather station' }))
}


const generatePrQueryList = async ({ plantList, startUnixTimestamp, endUnixTimestamp, window = 300 }) => {
  if (!startUnixTimestamp) throw new Error('startUnixTimestamp is required!')
  if (!endUnixTimestamp) throw new Error('endUnixTimestamp is required!')

  const startTime = Math.floor(startUnixTimestamp / window) * window 
  const endTime   = Math.ceil(endUnixTimestamp / window) * window 
  const plants = plantList || await getPlantList()
  const plantWiseDevices = await getPlantWiseDeviceList(plants)
  const prDevices = ['inverter', 'mfm', 'energy meter']

  const queryList = []
  plants.forEach(plantId => {
    const deviceList = plantWiseDevices[plantId];
    if (!deviceList || !deviceList.length) return;

    if (!deviceList.includes('weather station')) return;

    prDevices.forEach(device => {
      if (!deviceList.includes(device)) return;
      
      queryList.push({
        plantId,
        device,
        timeRange: [startTime, endTime]
      })
    })

  })

  return queryList
}

module.exports = {
  generateInsolationQueryList,
  generatePrQueryList
}