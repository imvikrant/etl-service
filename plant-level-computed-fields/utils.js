const { queryApi, aggregateApi } = require("../aggregate-api")
const _ = require('lodash');
const { startOf, Datetime, getUnixtime } = require("../src/helpers/date");
const { getPlantInfo } = require("../src/configuration");


const computeCumulative = (arr) => {
  let runningTotal = 0;

  return arr.map(row => {
    const { value } = row;

    runningTotal += Number(value)
    return {
      ...row,
      value: runningTotal
    }
  })
}


const groupData = (data) => {
  const groupedData = {}

  data.forEach((row) => {
    const { device_id, parameter } = row
    const id = `${device_id}-${parameter}`;

    if (!groupedData[id]) {
      groupedData[id] = []
    }

    groupedData[id].push(row)
  })

  const grouped = Object.values(groupedData)
  const sortedGroups = grouped.map(g => _.sortBy(g, ['timestamp']))

  return sortedGroups;
}


const getCumulativeDailySum = async ({ plantId, device, equipmentIds, fields, timeRange }, timeInterval, level) => {
  const window                       = 300;
  const [startUnixTime, endUnixTime] = timeRange
  const { timezone } = getPlantInfo({ plantId })

  const startTime                    = getUnixtime(startOf(Datetime(startUnixTime, timezone), 'day'))
  const cumulativeEndTime            = Math.ceil(startUnixTime / window) * window

  if (cumulativeEndTime > endUnixTime) return;

  const data               = []
  const aggregateFieldsMap = Object.fromEntries(fields.map(field => [field, 'sum']))

  const totalData = await aggregateApi({ device, plantId, equipmentIds, aggregateFieldsMap, timeRange: [startTime, cumulativeEndTime] }, timeInterval, level)
  data.push(...totalData.map(d => ({ ...d, timestamp: cumulativeEndTime })))

  if (cumulativeEndTime < endUnixTime) {
    const tempData = await queryApi({ plantId, device, equipmentIds, fields, timeRange: [cumulativeEndTime + window, endUnixTime] }, timeInterval, level)
    data.push(...tempData);
  }

  const sortedGroups     = groupData(data)
  const cumulativeGroups = sortedGroups.map(g => computeCumulative(g))

  return cumulativeGroups
}


const mergeCollectionByKey = (colA, colB, keyName) => {
  var merged = _.merge(_.keyBy(colA, keyName), _.keyBy(colB, keyName));
  return _.values(merged);
}


module.exports = {
  mergeCollectionByKey,
  getCumulativeDailySum
}
