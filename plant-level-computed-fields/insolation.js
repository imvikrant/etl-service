const { aggregateApi } = require('../aggregate-api');
const { getWsConfig, getEquipmentList, loadConfiguration } = require('../src/configuration');

const getPlantInsolation = async (query) => {
  const device = 'weather station'
  const { plantId, timeRange } = query
  const wsConfig = getWsConfig({ plantId })
  if (!wsConfig) return null;
  const { poaRefId, ghiRefId, radRefId, calculationType } = wsConfig;
  const equipmentList = getEquipmentList({ plantId, device })

  if (calculationType === 'average') {
    return aggregateApi({
      plantId,
      device,
      timeRange,
      equipmentIds      : equipmentList,
      aggregateFieldsMap: {
        'poa_insolation': 'average',
        'ghi_insolation': 'average',
        'rad_insolation': 'average'
      },
      groupBy: ['parameter', 'sensor_time'],
    }, 'minutely', 'device')
  }

  if (calculationType === 'count_poa') {
    const insolationData = []
    const poaData = poaRefId && await aggregateApi({
      plantId,
      device,
      timeRange,
      equipmentIds      : [poaRefId],
      aggregateFieldsMap: { 'poa_insolation': 'average' },
      groupBy           : ['parameter', 'sensor_time'],
    }, 'minutely', 'device')

    const ghiData = ghiRefId && await aggregateApi({
      plantId,
      device,
      timeRange,
      equipmentIds      : [ghiRefId],
      aggregateFieldsMap: { 'ghi_insolation': 'average' },
      groupBy           : ['parameter', 'sensor_time'],
    }, 'minutely', 'device')

    const radData = radRefId && await aggregateApi({
      plantId,
      device,
      timeRange,
      equipmentIds      : [radRefId],
      aggregateFieldsMap: { 'ghi_insolation': 'average' },
      groupBy           : ['parameter', 'sensor_time'],
    }, 'minutely', 'device')

    if (poaData) insolationData.push(...poaData)
    if (ghiData) insolationData.push(...ghiData)
    if (radData) insolationData.push(...radData)
    return insolationData;
  }

  return null
}


async function* generatePlantInsolationData(queryList) {
  const plants = queryList.map(query => query.plantId)
  await loadConfiguration({ plants })

  for (let query of queryList) {
    const data = await getPlantInsolation(query)
    if (!data) {
      yield null;
      continue;
    }
    yield data.map(obj => ({
      timestamp: obj.timestamp,
      plantId: obj.plant_id,
      device: obj.device,
      parameter: obj.parameter,
      value: obj.value
    }))
  }
}



module.exports = {
  generatePlantInsolationData
}