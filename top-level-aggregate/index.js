const { updateDailyData } = require("./update-daily-data")
const { updateMonthlyData } = require("./update-monthly-data")
const { updateYearlyData } = require("./update-yearly-data")

const aggregateTopLevelData = async (queryList) => {
  await updateDailyData(queryList)
  await updateMonthlyData(queryList)
  await updateYearlyData(queryList)
}

module.exports = {
  aggregateTopLevelData
}