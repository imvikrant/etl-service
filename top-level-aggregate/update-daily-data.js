const { splitQuery } = require("./../aggregate-api/utils/split-query")
const { getAggregateFieldMapping } = require("./../aggregate-api/utils/aggregate-query-parser")
const { aggregateApi } = require("./../aggregate-api/index.js")
const { insertAggregateData, insertPlantAggregateData } = require("../aggregate-api/insert-data")
const { santizeTimeRange } = require("./utils")
const { getPlantInfo } = require("../src/configuration/plant-information/plant-details")
const { getEquipmentList, loadConfiguration } = require("../src/configuration")
const { getComputedParameterList } = require("../src/configuration/parameter-information/computed-parameters")
const { getParameterList } = require("../src/configuration/parameter-information/parameters")

const overideAggregateMethods = {
  'inverter': {
    cuf: 'sum',
    ac_yield: 'sum',
    dc_yield: 'sum',
    pr: 'latest'
  },
  'mfm': {
    ac_yield: 'sum',
    cuf: 'sum',
    pr: 'latest'
  },
  'energy meter': {
    ac_yield: 'sum',
    cuf: 'sum',
    pr: 'latest'
  },
  'weather station': {
    poa_insolation: 'sum',
    ghi_insolation: 'sum',
  }
}

const parseQuery = (query) => {
  const { device, fields } = query;

  const aggregateFieldsMap = getAggregateFieldMapping(device, fields, overideAggregateMethods)

  return ({
    ...query,
    aggregateFieldsMap
  })
}

const sanitizeDailyAggregationQueries = (queries) => {
  return queries
    .map(query => {
      const { plantId, timeRange } = query;
      const { timezone } = getPlantInfo({ plantId }) || {}

      return { ...query, timeRange: santizeTimeRange(timeRange, 'day', timezone) }
    })
    .map(query => {
      const { plantId, device } = query;
      if (!query.equipmentIds) return { ...query, equipmentIds: getEquipmentList({ plantId, device }) }

      return { ...query };
    })
    .map(query => {
      const { device, fields } = query
      const computedFieldList = getComputedParameterList(device) || []
      const fieldList = getParameterList(device) || []

      return ({
        ...query,
        fields: fields || [...fieldList, ...computedFieldList]
      })
    })
    .filter(query => query.equipmentIds)
}

async function* generateAggregateFromPentaMinuteDeviceData(aggregateQueryList) {
  aggregateQueryList = aggregateQueryList.map(query => ({ ...query, groupBy: ['device_id', 'parameter'] }))

  for (let query of aggregateQueryList) {
    const [startUnixTime] = query.timeRange
    const data = await aggregateApi(query, 'minutely', 'device')

    yield data.map(({ plant_id: plantId, device, device_id: deviceId, parameter, value }) => (
      { timestamp: startUnixTime, plantId, device, deviceId, parameter, value }
    ))
  }
}

async function* generateAggregateFromPentaMinutePlantData(aggregateQueryList) {
  aggregateQueryList = aggregateQueryList.map(query => ({ ...query, groupBy: ['parameter'] }))

  for (let query of aggregateQueryList) {
    const [startUnixTime] = query.timeRange
    const data = await aggregateApi(query, 'minutely', 'plant')
    yield data.map(({ plant_id: plantId, device, parameter, value }) => (
      { timestamp: startUnixTime, plantId, device, parameter, value }
    ))
  }
}

const updateDailyDeviceData = async (queryList) => {
  for await (const data of generateAggregateFromPentaMinuteDeviceData(queryList)) {

    await insertAggregateData(data, 'daily')
  }
}

const updateDailyPlantData = async (queryList) => {
  for await (const data of generateAggregateFromPentaMinutePlantData(queryList)) {

    await insertPlantAggregateData(data, 'daily')
  }
}

const updateDailyData = async (queryList) => {
  const plants = queryList.map(query => query.plantId)
  await loadConfiguration({ plants })

  const sanitizedQueryList = sanitizeDailyAggregationQueries(queryList);
  const splitQueries = sanitizedQueryList.flatMap(query => splitQuery(query, 'day'))
    .map(query => parseQuery(query))

  await updateDailyDeviceData(splitQueries)
  console.log('Daily Device data updated!')
  await updateDailyPlantData(splitQueries)
  console.log('Daily Plant data updated!')
}

module.exports = {
  updateDailyData
}