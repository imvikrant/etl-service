const { startOf, Datetime, getUnixtime, endOf } = require("../src/helpers/date")

const santizeTimeRange = (timeRange, unit, timezone) => {
  const [start, end] = timeRange
  const startTime = Datetime(start, timezone) 
  const endTime = Datetime(end, timezone) 

  return [
    getUnixtime(startOf(startTime, unit)),
    getUnixtime(endOf(endTime, unit)),
  ]  
}

module.exports = {
  santizeTimeRange
}