const { aggregateTopLevelData } = require(".");
const { getPlantList } = require("../src/configuration/plants");
const { now, startOf, getUnixtime } = require("../src/helpers/date");
const { formUltimateTopLevelAggregateQueryParameterList } = require("./generate-query");


module.exports.run = async (event) => {
  const timezone           = event.timezone;
  const plantList          = getPlantList({ timezone })
  const currentTime        = now(timezone)
  const startUnixTimestamp = getUnixtime(startOf(currentTime, 'day'))
  const endUnixTimestamp   = startUnixTimestamp

  const queryData = await formUltimateTopLevelAggregateQueryParameterList({plantList, startUnixTimestamp, endUnixTimestamp })

  try {
    await aggregateTopLevelData(queryData) 
    return { message: 'Function executed successfully!!', event };
  } catch (e) {
    console.error(e)
    return { message: 'Function execution failed!', event, error: e };f
  }
};
