const { etl } = require('./index');
const { updatePlantData } = require('./plant-level-continuous-aggregate/update-plant-data');
const { aggregateTopLevelData } = require('./top-level-aggregate');
const { computeDataForDerivedParameters } = require('./plant-level-computed-fields');
const { formUltimateQueryParameterList } = require('./generate-query');


const aggregateRunner = async (queryParameterList, timezone) => {
  const etlOptions = {
    writeConcurrency: 5,
    writeBatchSize  : 10000,
    readBatchSize   : 20000,
  }
  const validPrDevicesSet = new Set(['inverter', 'mfm', 'energy meter'])

  const insolationQueryList = queryParameterList
    .filter(query => query.device == 'weather station')
    .map(({ plantId, device, timeRange }) => ({ plantId, device, timeRange }))

  const prQueryList = queryParameterList
    .filter(query => validPrDevicesSet.has(query.device))
    .map(({ plantId, device, timeRange }) => ({ plantId, device, timeRange }))

  const topLevelQueryList = [...queryParameterList]

  // Re-aggregate 5 minute device and plant data
  const { rowsRead, rowsWritten } = await etl(queryParameterList, etlOptions)
  console.log(`Aggregate Stats -> Read: ${rowsRead} | Wrote: ${rowsWritten}`)
  await updatePlantData(queryParameterList)  

  // Update derived parameters (pr, insolation)
  insolationQueryList.length && await computeDataForDerivedParameters(insolationQueryList)
  prQueryList.length && await computeDataForDerivedParameters(prQueryList)

  // Update daily, monthly, yearly data
  topLevelQueryList.length && await aggregateTopLevelData(queryParameterList)
}


const aggregateForAll = async (startUnixTimestamp, endUnixTimestamp) => {
  const window = 300 // 5 min
  const queryData = await formUltimateQueryParameterList({ startUnixTimestamp, endUnixTimestamp, window })
  await aggregateRunner(queryData)
}


module.exports = {
  aggregateRunner,
  aggregateForAll
}