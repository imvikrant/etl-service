const { computeDataForDerivedParameters } = require("../plant-level-computed-fields");
const { generateInsolationQueryList, generatePrQueryList } = require("../plant-level-computed-fields/generate-query");
const { getUnixtime, now } = require("../src/helpers/date");
const { formUltimatePlantAggregateQueryParameterList } = require("./generate-query");
const { updatePlantData } = require("./update-plant-data");


module.exports.run = async (event) => {
  const currentTime = now()
  const window = 300 // 5 min
  const startUnixTimestamp = Math.floor(getUnixtime(currentTime) / window) * window 
  const endUnixTimestamp = startUnixTimestamp  

  const queryData = await formUltimatePlantAggregateQueryParameterList({ startUnixTimestamp, endUnixTimestamp, window })
  const insolationQueryList = await generateInsolationQueryList({ startUnixTimestamp, endUnixTimestamp })
  const prQueryList = await generatePrQueryList({ startUnixTimestamp, endUnixTimestamp })

  try {
    await updatePlantData(queryData)
    await computeDataForDerivedParameters(insolationQueryList)
    await computeDataForDerivedParameters(prQueryList)
    return { message: 'Function executed successfully!!', event };
  } catch (e) {
    console.error(e)
    return { message: 'Function execution failed!', event, error: e };
  }
};