const { loadConfiguration, getEquipmentList } = require("../src/configuration")
const { generateMinutelyPlantAggregateData } = require("../aggregate-api")
const { getAggregateFieldMapping } = require("../aggregate-api/utils/aggregate-query-parser")
const { insertPlantAggregateData } = require("../aggregate-api/insert-data")
const { getReferenceEquipmentList } = require("../src/configuration/device-information")
const { getParameterList } = require("../src/configuration/parameter-information/parameters")
const { getComputedParameterList } = require("../src/configuration/parameter-information/computed-parameters")

const overideAggregateMethods = {
  'inverter': {
    cuf: 'average',
    ac_yield: 'average',
    dc_yield: 'average',
  },
  'mfm': {
    ac_yield: 'average',
    cuf: 'average',
  },
  'energy meter': {
    ac_yield: 'average',
    cuf: 'average',
  }
}

const addReferenceDeviceIds = (queryList) => {
  return queryList.map(query => {
    const { plantId, device  } = query 
    const equipmentIds = getEquipmentList({ plantId, device })
    const referenceDeviceIds = getReferenceEquipmentList({ plantId, device }) 
    
    return ({
      ...query,
      equipmentIds: referenceDeviceIds || equipmentIds || null
    })
  })
}

const addFields = (queryList) => {
  return queryList.map(query => {
    const { device, fields } = query 
    
    const computedFieldList = getComputedParameterList(device) || []
    const fieldList = getParameterList(device) || []
    
    return ({
      ...query,
      fields: fields || [ ...fieldList, ...computedFieldList ]
    })
  })
}

const parseQuery = (query) => {
  const { device, fields } = query;

  const aggregateFieldsMap = getAggregateFieldMapping(device, fields, overideAggregateMethods) 
  
  return ({
    ...query,
    aggregateFieldsMap
  })
}


const updatePlantData = async (queryList) => {   
  const plants = queryList.map(query => query.plantId)
  await loadConfiguration({plants})

  let modifiedQueryList = addReferenceDeviceIds(queryList)
  modifiedQueryList = addFields(modifiedQueryList)
  const parsedQueryList = modifiedQueryList.filter(query => query.equipmentIds).map(parseQuery)

  for await (const data of generateMinutelyPlantAggregateData(parsedQueryList)) {
    const dataToInsert = data.map(obj => ({
      timestamp: obj.timestamp,
      plantId  : obj.plant_id,
      device   : obj.device,
      parameter: obj.parameter,
      value    : obj.value
    }))

    await insertPlantAggregateData(dataToInsert, 'minutely')
  }
  console.log('Plant Data Updated')
}


module.exports = {
  updatePlantData
}