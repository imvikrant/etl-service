const { formUltimateQueryParameterList } = require("./generate-query");
const { etl } = require("./index");
const { getUnixtime, now } = require("./src/helpers/date");


module.exports.run = async (event) => {
  const currentTime = now()
  const window = 300 // 5 min
  const endUnixTimestamp   = Math.floor(getUnixtime(currentTime) / window) * window 
  const startUnixTimestamp = endUnixTimestamp - window 

  const queryData = await formUltimateQueryParameterList({ startUnixTimestamp, endUnixTimestamp, window })

  try {
    const { rowsRead, rowsWritten } = await etl(queryData, { cacheRemote: true })
    console.log(`Stats -> Read ${rowsRead} rows | Wrote ${rowsWritten} rows`)
    return { message: 'Function executed successfully!!', event };
  } catch (e) {
    console.error(e)
    return { message: 'Function execution failed!', event, error: e };
  }
};